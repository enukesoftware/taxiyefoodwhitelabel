import { Constants } from "../utils";
import { AsyncStorage } from "react-native";

async function callApi(urlString, header, body, methodType, isMultipart) {
    console.log("----------- Api request is----------- ");
    console.log("url string " + urlString);
    console.log("header " + JSON.stringify(header));
    console.log("body " + JSON.stringify(body));
    console.log("methodType " + methodType)

    return fetch(urlString, {
        method: methodType,
        headers: header,
        body: isMultipart ? body : methodType == "POST" ? JSON.stringify(body) : null
    })
        .then(response => {
            console.log("-----------Response is----------- ")
            console.log(response)
            if (response.status == 200) {
                // console.log("RES:"+JSON.stringify(response.json()))
                return response.json()
            } else {
                throw new Error(" status code " + response.status)
            }
        })
        .then((responseJson) => {
            return responseJson
        })
        .catch((error) => {
            throw error
        })
}

async function fetchApiData(urlString, body, methodType) {
    let header = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        // "Authorization": 'Bearer ' + Constants.userToken,
        // token: Constants.dailyToken,
        // agency_Id: 0
    }
    return callApi(urlString, header, body, methodType,false)

}

async function fetchApiDataForMultipart(urlString, body, methodType) {
    let header = {
        "Accept": "application/json",
        "Content-Type": "multipart/form-data",
    }
    return callApi(urlString, header, body, methodType,true)

}

export async function getRestaurantList(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}

export async function getCuisinesList(url) {
    return fetchApiData(url, '', Constants.API_METHOD.get)
}

export async function getRestaurantDetail(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}

export async function bookTable(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}

export async function bookingHistory(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}

export async function cancelBooking(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}

export async function checkNumber(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}

export async function signInApi(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}

export async function sendOtpApi(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}

export async function signUpApi(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}

export async function forgotPasswordApi(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}

export async function profileUpdateApi(url, param) {
    return fetchApiDataForMultipart(url, param, Constants.API_METHOD.post)
}

export async function addressApi(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}

export async function dealsApi(url) {
    return fetchApiData(url, '', Constants.API_METHOD.get)
}

export async function applyCouponApi(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}

export async function addOrderApi(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}

export async function getOrderList(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}

export async function OrderDetailApi(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}

export async function OrderCancleApi(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}

export async function directionApi(url){
    return fetchApiData(url,'',Constants.API_METHOD.get)
}

export async function updateDeviceTokenApi(url,param){
    return fetchApiData(url,param,Constants.API_METHOD.post)
}

export async function deleteAddressApi(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}

export async function logoutApi(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}

export async function changePasswordApi(url, param) {
    return fetchApiData(url, param, Constants.API_METHOD.post)
}
