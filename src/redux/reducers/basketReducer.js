import ActionTypes from '../actions/types'

export const basketReducer = (state = null, action) => {
    switch (action.type) {
        case ActionTypes.ADD_ITEM_IN_BASKET:
            return action.item
        case ActionTypes.REMOVE_ITEM_FROM_BASKET:
            return action.item
        case ActionTypes.BASKET_DATA:
            return state
        default:
            return state
    }
}

// export const removeItemFromBasketReducer = (state = null, action) => {
//     switch (action.type) {
//         case ActionTypes.USER_DATA:
//             return action.userData
//         default:
//             return state
//     }
// }