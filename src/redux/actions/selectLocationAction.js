import ActionTypes from './types'

export const locationSelected = (location) =>{
    // console.log('ACTION_CONNECTED')
    return{
        type:ActionTypes.LOCATION_SELECTED,
        location:location
    };
}

export const clearLocation = () =>{
    // console.log('ACTION_CONNECTED')
    return{
        type:ActionTypes.CLEAR_LOCATION,
    };
}