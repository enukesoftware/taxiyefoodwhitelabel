import { Constants } from '../utils'
import { updateDeviceTokenApi } from '../services/APIService'


export function checkRedux(state, action, value) {
    console.log("COMMON_FUNCTION_STATE:" + JSON.stringify(state))
    console.log("COMMON_FUNCTION_VALUE:" + value)

}

export function initializeBasket(basketData, action, restDetail, product) {
    action(null)
    addItemInBasket(basketData, action, restDetail, product)
}

export function addItemInBasket(basketData, action, restDetail, product) {

    const restId = restDetail.id
    const totalQuantity = getTotalQuantity(basketData, product)
    const totalPrice = this.getTotalPrice()
    const totalCgst = quantity * parseInt(restDetail.cgst)
    const totalSgst = quantity * parseInt(restDetail.sgst)
}


function getTotalQuantity(basketData, product) {
    let products = []
    let quantity = 0
    if (basketData && basketData.products && basketData.products.length > 0) {
        products = [...basketData.products, ...[product]]
    } else {
        products = [product]
    }
    console.log("PRODUCTS:", JSON.stringify(products))
    products.forEach(element => {
        quantity += element.quantity
    });
}

function _changeLanguage() {
    // let lan = strings.getLanguage()
    // console.log("LANGUAGE:"+lan)
    // if(lan == 'en'){
    //   strings.setLanguage('so');
    //   this.setState({});
    // }else{
    //   strings.setLanguage('en');
    //   this.setState({});
    // }

}


export async function callUpdateDeviceTokenApi() {
    console.log("CALL_UPDATE_TOKEN_API")
    let isInternetConnected = store.getState().internet;
    let userData = store.getState().userData;
    let isLogin = store.getState().isLogin;
    let deviceToken = store.getState().deviceToken;
    // console.log("TOKEN_USER_DATA:"+JSON.stringify(userData))
    // console.log("TOKEN_DEVICE_TOKEN:"+JSON.stringify(deviceToken))
    if (!isInternetConnected || !isLogin || !userData || !deviceToken) {
        // alert(strings.message_lno_internet)
        return
    }


    const param = {
        id: userData.id,
        device_token: deviceToken,
    }

    let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.updateDeviceToken

    console.log("DEVICE_TOKEN_URL:" + url)
    console.log("DEVICE_TOKEN_REQ:" + JSON.stringify(param))

    updateDeviceTokenApi(url, param).then(res => {

        console.log("UPDATE_TOKEN_API_RES:" + JSON.stringify(res))
        // this.setState({
        //     isLoading: false,
        // })

        if (res && res.success) {
            // this.setState({
            //     isLoading: false,
            // }, () => {
            //     if (res.data) {
            //         AsyncStorage.setItem(Constants.STORAGE_KEY.userData, JSON.stringify(res.data));
            //         AsyncStorage.setItem(Constants.STORAGE_KEY.isLogin, JSON.stringify({ isLogin: true }));
            //         this.props.userDataAction(res.data)
            //         this.props.isLoginAction(true)
            //         NavigationService.navigate(previousPage)
            //     }

            // })
        } else {
            // this.setState({
            //     isLoading: false,
            // })
            // if (res && res.message) {
            //     alert(res.message)
            // }
        }

    }).catch(err => {
        // this.setState({
        //     isLoading: false,
        // })
        // setTimeout(() => {
        //     if (err) {
        //         alert(JSON.stringify(err));
        //     }
        // }, 100);
    });
}

