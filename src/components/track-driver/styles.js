import { StyleSheet } from 'react-native';
import {Constants} from '../../utils'


export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        // height: '100%',
        width: '100%',
    },
    textTitle: {
        fontSize: 20,
        color: Constants.color.white,
        marginLeft: 15,
    },
    map: {
        // ...StyleSheet.absoluteFillObject,
        // position: 'absolute',
        // top: 0,
        // left: 0,
        // width: '100%',
        // height: '100%',
        flex:1,
        width: '100%'
    },
    mapText:{
        borderWidth:1,
        borderColor:Constants.color.dark_gray,
        borderRadius:5,
        padding:5,
        backgroundColor:Constants.color.fontWhite
    }
})