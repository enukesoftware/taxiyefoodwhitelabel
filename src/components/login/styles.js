import { StyleSheet,Platform } from 'react-native';
import { Constants, Fonts } from '../../utils'


export default StyleSheet.create({
    container: {
        flex: 1,
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
        alignItems: "center",

    },
    toolbar: {
        marginTop:Constants.AppConstant.statusBarHeight,
        height: (Platform.OS === Constants.PLATFORM.ios) ? Constants.TOOLBAR_HEIGHT.ios : Constants.TOOLBAR_HEIGHT.android,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },

    loginForm: {
        padding: 10,
        flex: 9,
        alignItems: 'center',
        justifyContent: 'center'
    },
    viewHead: {
        backgroundColor: Constants.color.white,
        width: '100%',
        height: 2,
    },
    textInput: {
        fontSize: 16,
        height: 40,
        color: Constants.color.white,
        fontFamily: Fonts.Regular,
        width: '100%'
    },
    inputLayout: {
        width: '80%',
        marginHorizontal: 10
    },
    iconLeft: {
        height: 25,
        width: 25,
        margin: 5
    },
    viewInput: {
        paddingTop: 15,
        flexDirection: "row",
        alignItems: 'flex-end',
    },
    viewButton: {
        alignItems: 'center',
        justifyContent: "center",
        paddingTop: 20,
    },
    touchOpacity: {
        backgroundColor: Constants.color.primary,
        justifyContent: "center",
        alignItems: "center",
        width: '100%',
        margin: 10,
        padding: 15,
        borderRadius: 5
    },
    textButtonAdd: {
        color: Constants.color.white,
        fontSize: 18
    },
    textForgetPass: {
        color: Constants.color.white,
        fontSize: 16,
    },
    forgetTouch: {
        marginTop: 25,
        marginLeft: 5,
        alignSelf: 'flex-start'
    },

})