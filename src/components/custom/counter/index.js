import React, { Component } from 'react'
import { Platform, StyleSheet, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../../utils'
import { TextBold, TextRegular } from '../text';

class Counter extends Component {

    constructor() {
        super()
    }


    render() {
        let tStyle = {}
        let cStyle = {}
        let val = 0
        if (this.props.value) {
            try {
                val = parseInt(this.props.value)
            }
            catch (e) {
                val = 0
            }
            value = val
            if (isNaN(val)) return null
            if (val < 1) return null
            if (val > 9) value = "9+"
        }
        else {
            return null
        }
        if (this.props.tStyle) {
            tStyle = this.props.tStyle
        }
        if (this.props.cStyle) {
            cStyle = this.props.cStyle
        }
        return (
            <View style={[styles.container, cStyle]}>
                {/* <Text style={styles.myStyle}>1</Text> */}
                <TextBold title={value} textStyle={[styles.myStyle, tStyle]} />
            </View>
        );

    }
}


export default Counter



const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 24,
        width: 24,
        borderRadius: 12,
        backgroundColor: Constants.color.primary,
    },
    myStyle: {
        fontSize: Constants.fontSize.NormalX,
        color: Constants.color.white,
        alignSelf: 'center',
        paddingBottom: 2
    }

})