import React, { Component } from 'react'
import { Platform, StyleSheet, View, TouchableOpacity,Text,Image } from 'react-native';
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../text'

class CustomAddItem extends Component {


    constructor() {
        super()
    }


    renderQuantityView = () => {
        if (this.props.quantity == 0) {
            return (
                /* ---------- Add Button ---------- */
                <TouchableOpacity onPress={()=>this.props.addItem()}>
                    <TextRegular title={strings.add} textStyle={styles.addText} />
                </TouchableOpacity>
            )
        } else if (this.props.quantity > 0) {
            return (
                /* ---------- Item Counter ---------- */
                <View style={styles.counterView}>
                    <TouchableOpacity style={styles.buttonStyle} onPress={()=>this.props.removeItem((this.props.product)?this.props.product:null)}>
                        <Image source={Images.ic_minus} style={{width:16,height:16}}/>
                    </TouchableOpacity>
                    <TextRegular title={this.props.quantity} textStyle={styles.countText} />
                    <TouchableOpacity 
                    style={[styles.buttonStyle, { alignSelf: 'flex-end' }]} 
                    onPress={()=>this.props.addItem((this.props.product)?this.props.product:null)}>
                        <Image source={Images.ic_plus} style={{width:16,height:16}}/>
                    </TouchableOpacity>
                </View>
            )
        }

    }

    render() {
        return (
            <View style={styles.container}>

            {this.renderQuantityView()}

            </View>
        );
    };
}

export default CustomAddItem

const styles = StyleSheet.create({
    container: {
        width: 100,
        borderColor: Constants.color.gray,
        borderWidth: 1,
        borderRadius: 5,

    },
    addText: {
        textAlign: 'center',
        padding: 2,
        fontSize: Constants.fontSize.SmallX
    },
    counterView: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    countText: {
        width: 68,
        textAlign: 'center',
        // padding: 2,
        fontSize: Constants.fontSize.SmallXX
    },
    buttonStyle: {
       // width: '20%',
        width:16,
        overflow:'hidden',
        // padding:5,
        // height:18,
        // width:18,
        backgroundColor: Constants.color.primary,
        borderRadius: 5,
    },
    buttonTextStyle: {
        // paddingHorizontal:5,
        textAlign: 'center',
        color: Constants.color.fontWhite,
        fontWeight: 'bold',
        // fontSize:Constants.fontSize.SMA
    }
})