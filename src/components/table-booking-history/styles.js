import {Platform, StyleSheet } from 'react-native';
import { Fonts } from '../../utils/fonts'
import { Constants } from '../../utils'


export default StyleSheet.create({

    toolbar: {
        height: (Platform.OS === Constants.PLATFORM.ios) ? Constants.TOOLBAR_HEIGHT.ios : Constants.TOOLBAR_HEIGHT.android,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Constants.color.primary,
        shadowOffset: { width: 10, height: 2 },
        shadowOpacity: 0.2,
        elevation: 15
    },
    scrollView: {
        marginBottom: 5,
    },
    textTitle: {
        fontSize: 20,
        color: Constants.color.white,
        marginLeft: 15,
    },
    loginForm: {
        width: '100%',
        flex: 9.3
    },
    imgBack: {
        width: '100%',
        height: '100%'
    },
    textActionButton: {
        color: Constants.color.white,
        fontSize: 14, 
    },
    textActionButton2: {
        color: Constants.color.primary,
        fontSize: 14, 
    },
    textAmountProcessing: {
        color: Constants.color.primary,
        fontSize: 14,
        fontWeight: '500'
    },
    viewItems: {
        width: '70%',
        padding: 5,
    },
    textItemsAdd:{
        width: '100%',
        fontSize: 12,
        padding:3,
        color:'#181818'
    },
    textItems: {
        fontSize: 12,
        color:'#181818'
    },
    viewInputRow: {
        flexDirection: 'row',
        alignItems:"center",
        width: '100%',
        padding:3
    },
    textItemName: {
        color: Constants.color.black,
        padding:3,
        fontSize: 16
    },
    imgLogo: {
        resizeMode:'cover',
        width: 100,
        height: 100,
        borderRadius:50,
        // borderColor:'red',
        // borderWidth:1,
    },
    viewImg: {
        width: 100,
        height: 100,
        padding: 5,
        alignItems: "center",
        justifyContent: "center",
        alignSelf:'center'
    },
    mainCard: {
        borderColor: Constants.color.white,
        borderBottomColor: Constants.color.gray,
        borderWidth: 1.5,
        padding: 2,
        margin: 5,
        borderRadius: 5,
        backgroundColor: 'white',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.2,
        elevation: 5
    },
    buttonAction: {
        backgroundColor: Constants.color.primary,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    buttonAction2: {
        backgroundColor: Constants.color.white,
        borderColor:Constants.color.primary,
        borderWidth:2,
        padding: 4,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    buttonStatus: {
        marginHorizontal: 5,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: Constants.color.primary,
    },
    activityIndicatorView:{
        height: '100%',
        width: '100%',
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
    },
    activityIndicatorWrapper:{
        backgroundColor: Constants.color.gray,
        height: 100,
        width: 100,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        opacity: 0.8,
    }




})
