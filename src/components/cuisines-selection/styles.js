import { StyleSheet, Platform } from 'react-native';
import { Constants } from '../../utils'


export default StyleSheet.create({
    container: {
        flex: 1,
        // width:'100%',
    },
    toolbar: {
        height: (Platform.OS === Constants.PLATFORM.ios) ? Constants.TOOLBAR_HEIGHT.ios : Constants.TOOLBAR_HEIGHT.android,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Constants.color.primary,
        shadowOffset: { width: 10, height: 2 },
        shadowOpacity: 0.2,
        elevation: 15
    },
    scrollView: {
        marginBottom: 100,
    },
    itemTouchable: {
        flexDirection: 'row',
        padding: 10,
        alignItems: 'center'
    },
    itemText: {
        width: '90%',
    },
    itemImageView: {
        width: '10%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    checkImage: {
        height: 18,
        width: 18,
    },
    doneButtonView: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        padding: 12,
        backgroundColor: Constants.color.primary
    },
    doneButtonTouchable: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    doneButtonText: {
        color: Constants.color.fontWhite,
        fontSize: Constants.fontSize.NormalXX,
    },
    activityIndicatorView:{
        height: '100%',
        width: '100%',
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        // paddingBottom: '15%'
    },
    activityIndicatorWrapper:{
        // position:'absolute',
        // top:'35%',
        // left:'38%',
        backgroundColor: Constants.color.gray,
        height: 100,
        width: 100,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        opacity: 0.8,
    }
    

})