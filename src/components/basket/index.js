import React, { Component } from 'react'
import { Alert, Platform, TouchableOpacity, SafeAreaView, ScrollView, TouchableHighlight, View, Image, ActivityIndicator, AsyncStorage } from 'react-native';
import styles from './styles'
import { Icon, Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextInput } from 'react-native-gesture-handler';
import RadioButton from 'react-native-radio-button'
import { userDataAction, isLoginAction, addItemInBasketAction } from '../../redux/actions'
import { connect } from 'react-redux'
import { TextInputLayout } from 'rn-textinputlayout'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import DateTimePicker from "react-native-modal-datetime-picker";
import { applyCouponApi } from '../../services/APIService'
import { NavigationEvents } from "react-navigation"
import moment from "moment";
import CustomAddItem from '../custom/custom-add-item'


let subtotal = 0; let cgst = 0; let sgst = 0; let finalTotal = 0; let delFee = 0; packFee = 0; let packCharges = 0;
let coupancode = "";
class Basket extends Component {

  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      user_id: '',
      isLogin: false,
      ll: false,
      isPickupSelected: false,
      isDeliverySelected: false,
      isEarliestSelected: false,
      isLaterSelected: false,
      isEarliestShown: false,
      isDatePickerVisible: false,
      isTimePickerVisible: false,
      bookingDate: "",
      bookingTime: "",
      selectedProduct: null,
      couponAppliedCode: "",
      couponAppliedtype: -1,
      couponValue: "0",
      discount: 0,
      isCouponApplied: false,
      couponPercentage: 0,
    }

  }

  componentDidMount() {
    this._mountNewdata()
  }

  _mountNewdata() {
    if (this.props.userData) {
      const userData = this.props.userData
      userId = (userData.id) ? userData.id : ""
      isLogin = this.props.isLogin
      this.setState({
        user_id: userId,
        isLogin: isLogin
      })
    }
  }

  onBackClick = () => {
    const { navigation } = this.props;
    navigation.goBack();
  }

  getTotalPrice = (isQuntitiyIncluded, product) => {
    let productPrice = parseInt(product.cost)
    let addOnPrice = 0

    if (product.selectedAddOnsGroups) {
      product.selectedAddOnsGroups.forEach(element => {
        let addOns = product.selectedAddOns[element]
        addOns.forEach(item => {
          addOnPrice += parseInt(item.price)
        });
      });
    }

    if (isQuntitiyIncluded) {
      return (product.quantity * (productPrice + addOnPrice))

    } else {
      return (productPrice + addOnPrice)
    }


  }


  addItem = (product) => {
    let updatedQuantity = product.quantity + 1
    const restDetail = this.props.basketData.restDetail
    const restId = this.props.basketData.restId
    let totalQuantity = this.props.basketData.totalQuantity + 1
    let totalPrice = this.props.basketData.totalPrice + product.unitPrice
    let totalCgst = totalQuantity * parseInt(restDetail.cgst)
    let totalSgst = totalQuantity * parseInt(restDetail.sgst)
    let updatedProduct = product

    updatedProduct = {
      ...updatedProduct,
      quantity: updatedQuantity,
      productPrice: updatedQuantity * parseInt(product.unitPrice),
    }

    var alreadyAddedProductsInBasket = [...this.props.basketData.products]; // make a separate copy of the array
    let productIndex = -1
    alreadyAddedProductsInBasket.forEach((element, index) => {
      if (element.id === product.id) {
        productIndex = index
      }
    });

    // totalPrice = totalPrice + this.getTotalPrice(true)
    // console.log("TOTAL_PRICE:"+totalPrice)
    // totalCgst = totalQuantity * parseInt(restDetail.cgst)
    // totalSgst = totalQuantity * parseInt(restDetail.sgst)

    let products = []
    if (productIndex != -1) {
      // alreadyAddedProductsInBasket.splice(productIndex, 1);
      alreadyAddedProductsInBasket[productIndex] = updatedProduct
      products = alreadyAddedProductsInBasket
      // if (alreadyAddedProductsInBasket && alreadyAddedProductsInBasket.length > 0) {
      //     products = [...alreadyAddedProductsInBasket, ...[updatedProduct]]
      // } else {
      //     products = [updatedProduct]
      // }
    } else {
      if (alreadyAddedProductsInBasket && alreadyAddedProductsInBasket.length > 0) {
        products = [...alreadyAddedProductsInBasket, ...[updatedProduct]]
      } else {
        products = [updatedProduct]
      }
    }

    // let products = []
    // if (alreadyAddedProductsInBasket && alreadyAddedProductsInBasket.length > 0) {
    //     products = [...alreadyAddedProductsInBasket, ...[updatedProduct]]
    // } else {
    //     products = [updatedProduct]
    // }


    const basket = {
      restDetail: restDetail,
      restId: restId,
      totalQuantity: totalQuantity,
      totalPrice: totalPrice,
      totalCgst: totalCgst,
      totalSgst: totalSgst,
      products: products
    }

    this.props.addItemInBasketAction(basket)
    AsyncStorage.setItem(Constants.STORAGE_KEY.basketData, JSON.stringify(basket));

  }

  removeItem = (product) => {
    let updatedQuantity = product.quantity - 1
    if (updatedQuantity > 0) {
      const restDetail = this.props.basketData.restDetail
      const restId = this.props.basketData.restDetail.id
      const totalQuantity = this.props.basketData.totalQuantity - 1
      let totalPrice = this.props.basketData.totalPrice - product.unitPrice

      // parseInt(item.cost)
      const totalCgst = totalQuantity * parseInt(this.props.basketData.restDetail.cgst)
      const totalSgst = totalQuantity * parseInt(this.props.basketData.restDetail.sgst)
      let updatedProduct = product


      updatedProduct = {
        ...updatedProduct,
        quantity: updatedQuantity,
        productPrice: updatedQuantity * product.unitPrice,
      }



      var alreadyAddedProductsInBasket = [...this.props.basketData.products]; // make a separate copy of the array
      let productIndex = -1
      alreadyAddedProductsInBasket.forEach((element, index) => {
        if (element.id === product.id) {
          productIndex = index
          if (element.selectedAddOnsGroups && element.selectedAddOnsGroups.length > 0) {
            // totalPrice = this.props.basketData.totalPrice - this.getTotalPrice(false,element)
            updatedProduct = {
              ...updatedProduct,
              // productPrice:this.getTotalPrice(true,element),
              // unitPrice:this.getTotalPrice(false,element),
              selectedAddOns: element.selectedAddOns,
              selectedAddOnsGroups: element.selectedAddOnsGroups
            }
          }
        }
      });

      let products = []
      if (productIndex != -1) {
        // alreadyAddedProductsInBasket.splice(productIndex, 1);
        alreadyAddedProductsInBasket[productIndex] = updatedProduct
        products = alreadyAddedProductsInBasket
      } else {
        if (updatedQuantity > 0) {
          products = [...alreadyAddedProductsInBasket, ...[updatedProduct]]
        } else {
          products = alreadyAddedProductsInBasket
        }
      }

      const basket = {
        restDetail: restDetail,
        restId: restId,
        totalQuantity: totalQuantity,
        totalPrice: totalPrice,
        totalCgst: totalCgst,
        totalSgst: totalSgst,
        products: products
      }

      if (totalQuantity == 0) {
        this.props.addItemInBasketAction(null)
        AsyncStorage.removeItem(Constants.STORAGE_KEY.basketData);
      } else {
        this.props.addItemInBasketAction(basket)
        AsyncStorage.setItem(Constants.STORAGE_KEY.basketData, JSON.stringify(basket));
      }
    } else (
      this.clearItemFromBasket(product)
    )
  }

  clearItemFromBasket = (product) => {
    let productQuantity = product.quantity
    if (productQuantity == this.props.basketData.products.totalQuantity) {
      this.props.addItemInBasketAction(null)
      AsyncStorage.removeItem(Constants.STORAGE_KEY.basketData);
    } else {
      const restDetail = this.props.basketData.restDetail
      const restId = this.props.basketData.restDetail.id
      const totalQuantity = this.props.basketData.totalQuantity - productQuantity
      let totalPrice = this.props.basketData.totalPrice - product.productPrice

      // parseInt(item.cost)
      const totalCgst = totalQuantity * parseInt(this.props.basketData.restDetail.cgst)
      const totalSgst = totalQuantity * parseInt(this.props.basketData.restDetail.sgst)

      var alreadyAddedProductsInBasket = [...this.props.basketData.products]; // make a separate copy of the array
      let productIndex = -1
      alreadyAddedProductsInBasket.forEach((element, index) => {
        if (element.id === product.id) {
          productIndex = index
        }
      });

      if (productIndex != -1) {
        alreadyAddedProductsInBasket.splice(productIndex, 1);
      }


      let products = alreadyAddedProductsInBasket



      const basket = {
        restDetail: restDetail,
        restId: restId,
        totalQuantity: totalQuantity,
        totalPrice: totalPrice,
        totalCgst: totalCgst,
        totalSgst: totalSgst,
        products: products,

      }

      if (totalQuantity == 0) {
        this.props.addItemInBasketAction(null)
        AsyncStorage.removeItem(Constants.STORAGE_KEY.basketData);
      } else {
        this.props.addItemInBasketAction(basket)
        AsyncStorage.setItem(Constants.STORAGE_KEY.basketData, JSON.stringify(basket));
      }

    }

  }

  changeQuantity = key => value => {
    // console.log("CHANGE_QUANTITY:" + key + ' ' + JSON.stringify(value))
    switch (key) {
      case 'add':
        this.addItem(value)
        break
      case 'remove':
        this.removeItem(value)
        break

    }
  }

  renderBackButton() {
    if (Platform.OS === Constants.PLATFORM.ios) {
      return (
        <TouchableOpacity activeOpacity={0.8}
          onPress={() => this.onBackClick()}>
          <Image source={Images.ic_back_ios}
            style={{ marginLeft: 5, height: 22, width: 22, }}
          ></Image>
        </TouchableOpacity>
      )
    } else {
      return (
        <TouchableOpacity activeOpacity={0.8}
          onPress={() => this.onBackClick()}>
          <Image source={Images.ic_back_android}
            style={{ marginLeft: 15, height: 35, width: 25, }}
          ></Image>
        </TouchableOpacity>
      )
    }
  }


  renderToolbar() {
    return (
      <View style={GlobalStyle.toolbar}>
        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
          width: '60%'
        }
        }>
          {this.renderBackButton()}
          <TextBold title={
            this.props.navigation.getParam('NavigateFrom', '') === 'Order' ?
              strings.modify_order :
              strings.title_basket} textStyle={styles.textTitle} />

        </View>
        {< View style={styles.ViewLogin} >
          {this.state.isLogin ?
            null :
            <TouchableHighlight style={{
              marginRight: 15,
              backgroundColor: '#c97712',
              padding: 2,
              borderRadius: 25,
              borderWidth: 1,
              borderColor: 'white',
            }}
              underlayColor='transparent'
              onPress={() => {
                this.props.navigation.navigate('Login', { previousPage: 'Basket' })
              }}><View style={{
                flexDirection: 'row',
                paddingHorizontal: 8,
                justifyContent: 'center',
                alignItems: 'center'
              }}>
                <Image style={{ height: 10, width: 10, paddingLeft: 10 }} resizeMode='center' source={Images.person_white} />
                <TextBold title={strings.login} textStyle={styles.textLoginNavigation} />
              </View>
            </TouchableHighlight>}

        </View >}
      </View>

    )
  }

  pressButton = (index, text) => {
    // alert(index)
    switch (text) {
      case "cancle":
        // basket_items: basket_items.splice(index, 1);
        // this.setState({
        //     ll: !this.state.ll
        // })
        break;


    }

  }

  generateRequestObject() {
    console.log('ItemsBasket:', (JSON.stringify(this.props.basketData)))
    let itemkey = []
    this.props.basketData.products.forEach(element => {
      // console.log('ItemsDETE:' + (JSON.stringify(element)))
      let addON = [];
      if (element.selectedAddOns != null) {
        for (const [key, value] of Object.entries(element.selectedAddOns)) {
          console.log("Key+" + key, " value:" + JSON.stringify(value));

          value.forEach(element => {
            let obj = {
              id: element.id,
              product_id: element.product_id,
              item_name: element.item_name,
              name: element.name,
              option_item_id: element.option_item_id,
              option_group_id: element.option_group_id,
              price: element.price,
              type: element.type,
              required: element.required,
            }
            addON.push(obj)
          })

        }
      }
      // console.log('ItemsaddON:' + (JSON.stringify(addON)))
      let obj = {
        product_id: element.id,
        product_name: element.name,
        product_quantity: element.quantity,
        product_unit_price: element.unitPrice,
        product_total_price: element.productPrice,
        addons: addON,
        addons_list: addON
      }
      itemkey.push(obj)
    });

    let key = {
      user_id: this.state.user_id,
      order_id: "",
      order: {
        restaurant_id: this.props.basketData.restDetail.id,
        datetime: this.state.isLaterSelected ? this.state.bookingDate + " " + moment(this.state.bookingTime, ["h:mm A"]).format("HH:mm:ss") : "",
        item: itemkey,
        delivery_type: this.state.isDeliverySelected ? Constants.DELIVERY_TYPE.delivery : this.state.isPickupSelected ? Constants.DELIVERY_TYPE.pickup : "",
        delivery_address_id: "",
        sub_total_before_discount: this.props.basketData.totalPrice,
        discount: this.state.discount,
        sub_total: subtotal,
        delivery_fee: delFee,
        packaging_fees: this.props.basketData.restDetail.packaging_fees,
        cgst: cgst,
        sgst: sgst,
        total: finalTotal,
        coupancode: coupancode,
        user_payment_method: "",
      }
    }

    return key;
  }
  confirmSelections = () => {
    if (this.props.basketData == null) return null


    key = this.generateRequestObject()

    if (subtotal < parseInt(this.props.basketData.restDetail.min_order)) {
      alert(strings.error_min_order + parseInt(this.props.basketData.restDetail.min_order))
      return
    }

    if (!this.state.isPickupSelected && !this.state.isDeliverySelected) {
      alert(strings.delivery_type_error)
      return
    }
    if (!this.state.isEarliestSelected && !this.state.isLaterSelected) {
      alert(strings.delivery_time_error)
      return
    }
    if (this.state.isLaterSelected) {
      if (this.state.bookingDate == "") {
        alert(strings.date_picker_error)
        return
      }
      if (this.state.bookingTime == "") {
        alert(strings.time_picker_error)
        return
      }
    }
    if (this.state.isDeliverySelected) {
      this.props.navigation.navigate('SelectDeliveryAddress', {
        NavigateFrom: 'Basket',
        keys: key
      })
    }
    if (this.state.isPickupSelected) {
      this.props.navigation.navigate('Checkout', {
        NavigateFrom: 'Basket',
        KEY_ORDER: key
      })
    }
  }
  showDatePicker = () => {
    this.setState({ isDatePickerVisible: true });
  };



  hideDatePicker = () => {
    this.setState({ isDatePickerVisible: false });
  };
  showTimePicker = () => {
    if (this.state.bookingDate == "") {
      alert(strings.date_picker_error)

    } else {
      this.setState({ isTimePickerVisible: true });
    }
  };
  hideTimePicker = () => {
    this.setState({ isTimePickerVisible: false });
  };
  handleDatePicked = date => {
    this.setState({  //bookingDate:date.getFullYear().toString()+"/"+(date.getMonth() + 1).toString()+"/"+date.getDate().toString(),
      bookingDate: moment(date).format('YYYY-MM-DD')
    })

    this.hideDatePicker();
  };
  handleTimePicked = date => {

    this.setState({  //bookingDate:date.getFullYear().toString()+"/"+(date.getMonth() + 1).toString()+"/"+date.getDate().toString(),
      bookingTime: moment(date).format('hh:mm A')

    })
    //  console.warn(date)

    this.hideTimePicker();
  };

  // renderQuantity = () => {
  //     return (
  //         <CustomAddItem
  //             quantity={(this.state.quantity)?this.state.quantity:0}
  //             addItem={this.changeQuantity('add')}
  //             removeItem={this.changeQuantity('remove')} />
  //     )
  // }

  basketlist = () => {

    if (!(this.props.basketData
      && this.props.basketData.products
      && this.props.basketData.products.length > 0)) {
      return null
    }
    console.log("Basket_List:" + JSON.stringify(this.props.basketData.products))

    return this.props.basketData.products.map((item, index) =>
      (
        <View key={index} style={{ marginBottom: 10, backgroundColor: Constants.color.seperator_color, marginHorizontal: 20, padding: 5, }}>
          <View style={{flexDirection:'row',marginBottom:10,alignItems:'center'}}>
          <Image source={(item.image && item.image.location && item.image.location != "") ? { uri: item.image.location } : Images.no_product} style={styles.menuInfoImage}></Image>
          <TextBold title={item.name} numberOfLines={2} textStyle={{ textAlign: 'left',width:'78%', paddingHorizontal: 5 }} />
          <TouchableHighlight underlayColor='transparent'
              onPress={() => this.clearItemFromBasket(item)} style={{ width: '10%',height:30, alignItems: 'center' }}>
              <Image resizeMode={'center'} style={{ height: 20, width: 20,padding:5 }} source={Images.cancel}></Image>
            </TouchableHighlight>
          </View>
          <View style={styles.weekRow}>

            <View >
              <CustomAddItem
                product={item}
                quantity={(item.quantity) ? item.quantity : 0}
                addItem={this.changeQuantity('add')}
                removeItem={this.changeQuantity('remove')} />
            </View>

            {/* <TextBold title={item.name} numberOfLines={2} textStyle={{ textAlign: 'left', width: '30%', paddingHorizontal: 5 }} /> */}
            <TextRegular title={ ' X ' + (Constants.currency.dollar + item.cost)} textStyle={[{marginLeft:5, color: 'green', fontSize: Constants.fontSize.NormalX }]}></TextRegular>
            {/* <TouchableHighlight underlayColor='transparent'
              onPress={() => this.clearItemFromBasket(item)} style={{ width: '10%', alignItems: 'center' }}>
              <Image resizeMode={'center'} style={{ height: 20, width: 20 }} source={Images.cancel}></Image>
            </TouchableHighlight> */}
          </View>
          {(item.selectedAddOnsGroups && item.selectedAddOnsGroups.length > 0) ?
            <View style={{ marginTop: 2 }}>
              <TextBold title={strings.AddOn} textStyle={{ marginVertical: 5, fontSize: 16, color: Constants.color.black }} />
              {this._renderAddonItems(item.selectedAddOnsGroups, item.selectedAddOns, item.quantity)}
            </View> : null}

        </View>
      )
    )
  }


  _renderAddonItems(selectedAddOnsGroups, selectedAddOns, quantity) {
    let list = []
    selectedAddOnsGroups.forEach(element => {
      if (selectedAddOns[element]) {
        list = [...list, ...selectedAddOns[element]]
      }
    });

    // console.log("ADD_ON_LIST:"+JSON.stringify(list))
    return list.map((item, index) => (
      <View key={index} style={{ flexDirection: 'row', width: Constants.Screen.width - 50, marginVertical: 5 }} >
        <View style={{ width: '65%', }}>
          <TextRegular title={item.item_name} textStyle={{ fontSize: 16, textAlign: 'left' }} />
        </View>
        <View style={{ width: '25%', alignItems: 'flex-end' }}>
          <TextRegular title={quantity + " X " + Constants.currency.dollar + item.price} textStyle={{ fontSize: 16, textAlign: 'right' }} />
        </View>
      </View>
    ))
  }

  calculateTotal() {

    console.log("BASKET_CHECK:" + this.props.basketData)
    if (this.props.basketData == null) {
      return null
    }
    let dis = 0;
    let bef = 0;
    let delApplicableFee = 0;


    if (this.state.couponAppliedtype == 1) {
      dis = (this.state.couponValue * (parseInt(this.props.basketData.totalPrice))) / 100
    } else if (this.state.couponAppliedtype == 0) {
      dis = this.state.couponValue
    }
    if (dis != this.state.discount) {
      this.setState({
        discount: dis
      })
    }


    try {
      bef = parseInt(this.props.basketData.totalPrice);
      delFee = parseInt(this.props.basketData.restDetail.delivery_charge);
      delApplicableFee = parseInt(this.props.basketData.restDetail.delivery_applicable);
      packCharges = parseInt(this.props.basketData.restDetail.packaging_fees);
    }
    catch (e) { alert(e) }
    if (!this.state.isDeliverySelected) {
      delFee = 0
    }
    subtotal = bef - dis
    // console.warn(subtotal.toString())
    if (subtotal > delApplicableFee) {
      delFee = 0
    }

    cgst = (subtotal / 10)
    sgst = (subtotal / 10)

    finalTotal = (subtotal + delFee + cgst + sgst + packCharges)
  }

  renderProgressBar() {
    if (this.state.isLoading) {
      return (
        <View style={GlobalStyle.activityIndicatorView}>
          <View style={GlobalStyle.activityIndicatorWrapper}>
            <ActivityIndicator
              size={"large"}
              color={Constants.color.primary}
              animating={true} />
          </View>
        </View>
      )
    } else {
      return
    }
  }

  showCancleCouponDialog = () => {
    Alert.alert(
      strings.title_coupon_dialog,
      strings.coupon_cancel_dialog,
      [
        //   {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
        {
          text: strings.no,
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: strings.yes,
          onPress: () => this.clearCoupon()
        },
      ],
      { cancelable: false },
    );
  }

  clearCoupon() {
    coupancode = "";
    this.setState({
      couponValue: 0,
      couponAppliedtype: -1,
      isCouponApplied: false,
      couponPercentage: 0,
      discount: 0
    })
  }

  details = () => {
    if (this.props.basketData == null) return null

    let itemsAddition = [
      {
        name: strings.subtotal_without_discount,
        price: this.props.basketData.totalPrice,
        bcColor: Constants.color.seperator_color
      },

      {
        name: strings.discount,
        price: this.state.discount,
        bcColor: 'white'
      },
      {
        name: strings.subtotal,
        price: subtotal,
        bcColor: Constants.color.seperator_color
      },
      this.state.isDeliverySelected ?
        {
          name: strings.delivery_fee,
          price: delFee,
          bcColor: 'white'
        } : null,
      {
        name: strings.packing_charge,
        price: this.props.basketData.restDetail.packaging_fees,
        bcColor: 'white'
      },
      {
        name: strings.cgst + " 10.00%",
        price: cgst,
        bcColor: 'white'
      },
      {
        name: strings.sgst + " 10.00%",
        price: sgst,
        bcColor: 'white'
      }
    ]
    return itemsAddition.map((item, index) =>
      (item != null ?
        <View key={index} style={[styles.weekRow2, { backgroundColor: item.bcColor }]}>
          <View style={{ width: '70%', }} >
            <TextRegular title={item.name} textStyle={[styles.textInfo2, {}]} />
          </View>
          <View style={{ width: '30%', alignItems: 'flex-end' }} >
            <TextRegular title={'$' + item.price} textStyle={[styles.textInfo2, {}]} />
          </View></View> : null
      )
    )
  }

  callapplyCouponApi() {
    if (!this.props.internet) {
      alert(strings.message_lno_internet)
      return
    }
    if (this.props.basketData == null) return null
    const param = {
      restaurant_id: this.props.basketData.restDetail.id,
      coupon_code: this.state.couponAppliedCode,
    }
    this.setState({
      isLoading: true,
    })
    let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.applyCoupon

    applyCouponApi(url, param).then(res => {
      console.log("ApplyCoupun:" + JSON.stringify(res))

      if (res && res.success) {
        let d = 0;
        console.log("ApplyCoupun:" + JSON.stringify(res))
        coupancode = this.state.couponAppliedCode,
          this.setState({
            isLoading: false,
            couponValue: res.data.coupon_value,
            couponAppliedtype: res.data.type,
            isCouponApplied: true,
            couponAppliedCode: "",
          })
        // alert(res.message);
      } else {
        this.setState({
          isLoading: false,
        }, () => {
          if (res && res.error) {
            alert(res.error)
          } else {
            if (res.message)
              alert(res.message)
          }
        })
      }

    }).catch(err => {
      this.setState({
        isLoading: false,
      })
      setTimeout(() => {
        if (err) {
          alert(JSON.stringify(err));
        }
      }, 100);
    });

  }
  main = () => {
    return (
      <View style={{ flex: 1 }} >
        <NavigationEvents
          onWillFocus={() => {
            this.setState({ renderList: !this.state.renderList }, () => {
              this._mountNewdata()
            })
            //Call whatever logic or dispatch redux actions and update the screen!
          }}
        />
        <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
        {this.renderToolbar()}
        {this.props.basketData == null ?
          <View style={{ flex: 1, alignItems: "center", justifyContent: 'center' }}>
            <TextBold title={strings.empty_basket_message}
              textStyle={{ fontSize: 16, color: Constants.color.black }} />

            <View style={{ height: 200, width: 200, alignItems: "center", justifyContent: 'center', padding: 15 }}>
              <Image source={Images.ic_big_cart}
                style={{ height: '100%', width: '100%' }} />
            </View>

          </View>
          :
          <ScrollView contentInsetAdjustmentBehavior="automatic"
            keyboardShouldPersistTaps='always' style={{ marginBottom: 5 }}
            showsVerticalScrollIndicator={false}>

            <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center', }}>
              <View style={{ width: '50%', justifyContent: "center" }}>
                <TextBold title={strings.your_order} textStyle={[styles.textInfo, styles.textHead]} />
              </View>

              {/* Change Menu View  */}

              {this.props.navigation.getParam('NavigateFrom', '') === 'Order' ?
                <View style={{ width: '50%', alignItems: 'flex-end', justifyContent: "center" }}>
                  <TouchableOpacity
                    onPress={() => {
                      ////// here handle change menu
                    }}
                    style={{ borderColor: 'red', borderWidth: 2, marginVertical: 10, marginEnd: 25, padding: 10, borderRadius: 12, alignItems: "center", justifyContent: "center" }}>
                    <TextRegular textStyle={{ color: 'red' }} title={strings.change_menu} />
                  </TouchableOpacity>
                </View> : null
              }
            </View>
            {this.basketlist()}

            {this.state.isCouponApplied ?
              <View style={styles.apliedView}>
                <View style={{ width: "90%", justifyContent: 'center', }}>
                  <TextRegular title={strings.coupon_message + this.state.discount + "$ " + strings.coupon_message_concat}
                    textStyle={{ paddingHorizontal: 10 }} />
                </View>
                <View style={{ width: "10%", justifyContent: 'center', alignItems: 'center' }}>
                  <TouchableHighlight underlayColor='transparent'
                    onPress={() => {
                      this.showCancleCouponDialog()
                    }} style={{ width: '100%', alignItems: 'center' }}>
                    <Image resizeMode={'center'} style={{ height: 20, width: 20 }} source={Images.cancel}></Image>
                  </TouchableHighlight>
                </View>
              </View>
              :
              <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 25 }}>
                <View style={{ flexDirection: 'column' }}>
                  <View style={{
                    flexDirection: "row", alignItems: "center", justifyContent: 'center',
                  }}>
                    <Image source={Images.gift} style={{ width: 30, height: 30 }}></Image>
                    <TextInput
                      value={this.state.couponAppliedCode}
                      onChangeText={
                        (text) => {
                          this.setState({
                            couponAppliedCode: text
                          })
                        }
                      } style={{ width: (Constants.Screen.width * 0.6) - 30, height: 50, fontFamily: Fonts.Regular, padding: 15 }}
                      placeholder={strings.couponcode
                      }></TextInput>
                  </View>
                  <View style={{ width: '100%', height: 1, marginTop: 5, backgroundColor: '#808080' }}></View>
                </View>
                <TouchableHighlight underlayColor={Constants.color.primaryDark} onPress={() => {
                  if (this.state.couponAppliedCode == "" || this.state.couponAppliedCode == null) {
                    alert(strings.errorCouponCode)
                  } else { this.callapplyCouponApi() }
                }} style={{
                  height: 41, width: Constants.Screen.width * 0.3,
                  borderRadius: 10, borderColor: Constants.color.primary,
                  alignItems: 'center', justifyContent: "center",
                  backgroundColor: Constants.color.primary
                }}>
                  <TextBold title={strings.apply} textStyle={[styles.textInfo]} />
                </TouchableHighlight>
              </View>}




            <TextBold title={strings.additions_of_items} textStyle={styles.textHead} />

            {this.details()}

            <View style={styles.weekRow}>
              <View style={{ flex: 7 }} >
                <TextRegular title={strings.total}
                  textStyle={[{ fontSize: 16, color: Constants.color.black, marginHorizontal: 15, marginVertical: 15, }]} ></TextRegular>
              </View>
              <View style={{ flex: 3, alignItems: 'flex-end' }} >
                <TextRegular title={'$' + finalTotal.toFixed(2)}
                  textStyle={[{ fontSize: 16, color: Constants.color.black, marginHorizontal: 15, marginVertical: 15, }]} ></TextRegular>
              </View></View>


            <TextBold title={strings.delivery_type} textStyle={[styles.textInfo, styles.textHead,]} />


            <View style={styles.rowCheckbox}>

              <RadioButton
                outerColor={Constants.color.primary}
                innerColor={Constants.color.primary}
                size={12}
                onPress={() => this.setState({
                  isPickupSelected: true,
                  isDeliverySelected: false,
                  isEarliestShown: true
                })}
                isSelected={this.state.isPickupSelected}
              />

              <TextRegular title={strings.pick_up_location} textStyle={[styles.textInfo, styles.textHead2,]} ></TextRegular>
              <RadioButton
                size={12}
                outerColor={Constants.color.primary}
                innerColor={Constants.color.primary}
                onPress={() => this.setState({
                  isDeliverySelected: true,
                  isPickupSelected: false,
                  isEarliestShown: true
                })}
                isSelected={this.state.isDeliverySelected}
              />

              <TextRegular title={strings.drop_location} textStyle={[styles.textInfo, styles.textHead2,]} ></TextRegular>
            </View>


            {this.state.isEarliestShown ?
              <View style={styles.columnCheckbox}>
                <View style={styles.rowCheckbox}>
                  <RadioButton
                    outerColor={Constants.color.primary}
                    size={12}
                    innerColor={Constants.color.primary}
                    onPress={() => this.setState({
                      isEarliestSelected: true,
                      isLaterSelected: false,
                      bookingTime: "",
                      bookingDate: ""
                    })}
                    isSelected={this.state.isEarliestSelected}
                  />

                  <TextRegular title={strings.as_soon_as} textStyle={[styles.textInfo, styles.textHead2,]} ></TextRegular>
                </View>

                <View style={styles.rowCheckbox}>

                  <RadioButton
                    outerColor={Constants.color.primary}
                    size={12}
                    innerColor={Constants.color.primary}
                    onPress={() => this.setState({
                      isEarliestSelected: false,
                      isLaterSelected: true,
                    })}
                    isSelected={this.state.isLaterSelected}
                  />

                  <TextRegular title={strings.later} textStyle={[styles.textInfo, styles.textHead2,]} ></TextRegular>
                </View>

              </View> :
              null
            }
            {
              this.state.isLaterSelected ?
                <View>
                  {/*Booking date*/}
                  <View
                    style={styles.viewInput}>
                    <Image source={Images.calender} style={styles.iconLeft}></Image>
                    <TouchableOpacity style={styles.inputLayout}
                      onPress={() => { this.showDatePicker() }}>
                      <TextInputLayout
                        focusColor={Constants.color.black}>
                        <TextInput
                          pointerEvents="none"
                          editable={false}
                          value={this.state.bookingDate}
                          style={styles.textInput}
                          placeholder={strings.hint_booking_date} />
                      </TextInputLayout>
                    </TouchableOpacity>
                    <DateTimePicker
                      isVisible={this.state.isDatePickerVisible}
                      mode="date"
                      minimumDate={new Date()}
                      maximumDate={new Date(moment(new Date()).add(7, 'day').format('YYYY-MM-DD'))}
                      onConfirm={this.handleDatePicked}
                      onCancel={this.hideDatePicker}
                    />

                  </View>

                  {/*Booking time*/}
                  <View style={styles.viewInput}>
                    <Image source={Images.clock} style={styles.iconLeft}></Image>
                    <TouchableOpacity style={styles.inputLayout}
                      onPress={() => { this.showTimePicker() }}>
                      <TextInputLayout
                        focusColor={Constants.color.black}>
                        <TextInput
                          pointerEvents="none"
                          editable={false}
                          is24Hour={false}
                          value={this.state.bookingTime}
                          style={styles.textInput}
                          placeholder={strings.hint_booking_time} />
                      </TextInputLayout>
                    </TouchableOpacity>
                    <DateTimePicker
                      isVisible={this.state.isTimePickerVisible}
                      mode="time"
                      titleIOS={strings.timer_title}
                      onConfirm={this.handleTimePicked}
                      onCancel={this.hideTimePicker}
                    />

                  </View></View> : null
            }
            <TouchableOpacity style={{
              padding: 20,
              flex: 1,
              backgroundColor: Constants.color.primary,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 10,
              margin: 25,
              borderColor: Constants.color.primary,
            }} onPress={() => {
              if (this.state.isLogin) {
                this.confirmSelections()
              } else {
                this.props.navigation.navigate('Login', { previousPage: 'Basket' })
              }
            }}>
              <TextRegular title={
                !this.state.isLogin ? strings.login_to_place_order
                  : this.state.isDeliverySelected ?
                    strings.drop_location_button :
                    strings.pick_up_location_button} textStyle={[styles.textTitle, { fontSize: 16 }]}></TextRegular>
            </TouchableOpacity>
          </ScrollView>
        }

        {this.renderProgressBar()}
      </View>
    )
  }

  render() {
    this.calculateTotal()
    return this.main();
  }

}

function mapStateToProps(state) {
  // console.log("BASKET_STATE:" + JSON.stringify(state.basketData))
  return {
    userData: state.userData,
    internet: state.internet,
    isLogin: state.isLogin,
    basketData: state.basketData
  }
}

export default connect(mapStateToProps, { isLoginAction, userDataAction, addItemInBasketAction })(Basket)
