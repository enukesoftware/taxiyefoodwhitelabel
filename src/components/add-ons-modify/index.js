import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Platform, SafeAreaView, TouchableHighlight, Image, View, TouchableOpacity, Text, Alert,AsyncStorage } from 'react-native';
import styles from './styles'
import { Constants, strings, Images, Fonts, GlobalStyle, initializeBasket, addItemInBasket } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import { TextInputLayout } from 'rn-textinputlayout';
import { ScrollView } from 'react-native-gesture-handler';
import CustomAddItem from '../custom/custom-add-item'
import CheckBox from 'react-native-check-box'
import RadioButton from 'react-native-radio-button'
import NavigationService from '../../services/NavigationServices'
import { addItemInModifyBasketAction, removeItemFromBasketAction } from '../../redux/actions'


class AddOnsModify extends Component {

    static navigationOptions = {
        header: null
    };

    constructor() {
        super()
    }

    state = {
        restDetail: null,
        menuName: "",
        quantity: 1,
        selectedAddOns: null,
        selectedAddOnsGroups: [],
        product: null,
       
    }

    componentDidMount() {
        const { navigation } = this.props;
        const menuName = navigation.getParam('menuName', "");
        let product = navigation.getParam('product', null);
        const restDetail = navigation.getParam('restDetail', null);
        let quantity = 1
        let selectedAddOns = []
        let selectedAddOnsGroups = []

        if (this.props.basketData) {
            if (this.props.basketData.products) {
                this.props.basketData.products.forEach(element => {
                    if (element.id == product.id) {
                        product = element
                        quantity = element.quantity
                        if (element.selectedAddOns) {
                            selectedAddOns = element.selectedAddOns
                        }
                        if (element.selectedAddOnsGroups) {
                            selectedAddOnsGroups = element.selectedAddOnsGroups
                        }
                    }
                });
            }
        }

        console.log("MODUselectedAddOns:" + JSON.stringify(selectedAddOns))
        console.log("MODUproduct" + JSON.stringify(product))
        console.log("MODUquantity:" + JSON.stringify(quantity))
        console.log("MODUselectedAddOnsGroups:" + JSON.stringify(selectedAddOnsGroups))

        this.setState({
            menuName: menuName,
            product: product,
            restDetail: restDetail,
            quantity: quantity,
            selectedAddOns: selectedAddOns,
            selectedAddOnsGroups: selectedAddOnsGroups,
        }, () => {
            // this.callGetRestaurantDetailApi()
        })
    }

    addItem = () => {
        this.setState(prevState => ({
            quantity: prevState.quantity + 1
        }))
        // this.setState({
        //     quantity: this.props.value.quantity
        // })

        // if(this.props.value.addons && this.props.value.addons.length>0){
        //     NavigationService.navigate('AddOns',{menuName:this.props.menuName,product:this.props.value})
        // }


        // console.log("ADD:" + this.props.value.quantity)
    }

    removeItem = () => {
        if (this.state.quantity > 1) {
            this.setState(prevState => ({
                quantity: prevState.quantity - 1
            }))
        }
        // if (this.props.value.quantity != 0) {
        //     this.props.value.quantity = this.props.value.quantity - 1
        //     this.setState({
        //         quantity: this.props.value.quantity
        //     })
        // }
    }

    changeQuantity = key => value => {
        console.log("KEY:" + key + " VALUE:" + value)
        switch (key) {
            case 'add':
                this.addItem()
                break
            case 'remove':
                this.removeItem()
                break

        }
    }

    // isAddOnExistInSelectedAddOnGroup = (addOn, groupId) => {
    //     this.state.selectedAddOns[groupId].forEach(element => {
    //         if (element.option_item_id === addOn.option_item_id) {
    //             return true
    //         }
    //     });
    //     return false
    // }

    contains(list, obj) {

        // console.log("COMPARE_OBJ:" + JSON.stringify(obj))
        // console.log("COMPARE_LIST:" + JSON.stringify(list))
        var i = list.length;
        while (i--) {
            if (list[i].option_item_id == obj.option_item_id) {
                // console.log("ADD_ON_ITEM_EXIST:" + i)
                return i;
            }
        }

        // console.log("ADD_ON_ITEM_NOT_EXIST:-1")
        return -1;
    }

    addAddOnInList = (addOn) => {
        // console.log("ADD_ON:" + JSON.stringify(addOn))
        // console.log("ADD_ON_LIST:" + JSON.stringify(this.state.selectedAddOns))
        const groupId = addOn.id
        if (addOn.type === 'checkbox') {
            if (this.state.selectedAddOns == null) {
                const item = { [groupId]: [addOn] }
                // console.log("ADD_ON_ITEM:" + JSON.stringify(item))
                this.setState({
                    selectedAddOns: item,
                    selectedAddOnsGroups: [groupId]
                }, () => {
                    console.log("ADD_ON_FIRST:" + JSON.stringify(this.state.selectedAddOns))
                })
            } else {
                if (this.state.selectedAddOns[groupId]) {
                    /* ---------- If AddOn Group Exist in Selection ---------- */
                    const index = this.contains(this.state.selectedAddOns[groupId], addOn)
                    if (index != -1) {
                        /* ---------- If AddOn exist in Group then remove it---------- */

                        var array = [...this.state.selectedAddOns[groupId]];
                        array.splice(index, 1);
                        const item = { [groupId]: array }

                        if (array.length > 0) {
                            this.setState({
                                selectedAddOns: { ...this.state.selectedAddOns, ...item }
                            }, () => {
                                console.log("ADD_ON_ALREADY_EXIST_THEN_REMOVED1:" + JSON.stringify(this.props.selectedAddOns))
                            })
                        } else {
                            let updateGroups = null
                            console.log("GROUPS:" + JSON.stringify(this.state.selectedAddOnsGroups))
                            var array = [...this.state.selectedAddOnsGroups]; // make a separate copy of the array
                            var indexOfGroup = array.indexOf(groupId)
                            if (indexOfGroup !== -1) {
                                array.splice(indexOfGroup, 1);
                                this.setState({
                                    selectedAddOnsGroups: array
                                }, () => {
                                    this.state.selectedAddOnsGroups.forEach(element => {
                                        if (this.state.selectedAddOns[element] && this.state.selectedAddOns[element].length > 0) {
                                            if (updateGroups == null) {
                                                updateGroups = { [element]: this.state.selectedAddOns[element] }
                                            }
                                            else {
                                                updateGroups = { ...updateGroups, ...{ [element]: this.state.selectedAddOns[element] } }
                                            }
                                        }
                                    });
                                    this.setState({
                                        selectedAddOns: updateGroups
                                    }, () => {
                                        console.log("ADD_ON_ALREADY_EXIST_THEN_REMOVED2:" + JSON.stringify(this.state.selectedAddOns))
                                    })
                                });


                            }

                        }


                    } else {
                        /* ---------- If AddOn Not exist in Group then add it ---------- */
                        const updatedGroup = [...this.state.selectedAddOns[groupId], ...[addOn]]
                        const item = { [groupId]: updatedGroup }
                        console.log("AFTER_ADD_ITEM:" + JSON.stringify(item))
                        this.setState({
                            selectedAddOns: { ...this.state.selectedAddOns, ...item }
                        }, () => {
                            console.log("ADD_ON_NOT_EXIST_IN_GROUP_THEN_ADDED:" + JSON.stringify(this.state.selectedAddOns))
                        })
                    }

                } else {
                    const item = { [groupId]: [addOn] }
                    console.log("ADD_ON_NEW_GROUP:" + JSON.stringify(item))
                    this.setState({
                        selectedAddOns: { ...this.state.selectedAddOns, ...item },
                        selectedAddOnsGroups: [...this.state.selectedAddOnsGroups, ...[groupId]]
                    }, () => {
                        console.log("ADD_ON_GROUP_NOT_EXIST_THEN_ADDED:" + JSON.stringify(this.state.selectedAddOns))
                    })
                    // this.setState(prevState => ({
                    //     selectedAddOns: { ...prevState.selectedAddOns, ...item }
                    // },()=>{
                    //     console.log("ADD_ON_GROUP_NOT_EXIST_THEN_ADDED:"+JSON.stringify(this.state.selectedAddOns))
                    // }))
                }
            }
        } else if (addOn.type === 'radio') {
            if (this.state.selectedAddOns == null) {
                const item = { [groupId]: [addOn] }
                this.setState({
                    selectedAddOns: item,
                    selectedAddOnsGroups: [groupId]
                }, () => {
                    console.log("ADD_ON_FIRST_RADIO:" + JSON.stringify(this.state.selectedAddOns))
                })
            } else {
                const item = { [groupId]: [addOn] }
                console.log("ADD_ON_NEW_GROUP_RADIO:" + JSON.stringify(item))
                this.setState({
                    selectedAddOns: { ...this.state.selectedAddOns, ...item },
                    selectedAddOnsGroups: [...new Set([...this.state.selectedAddOnsGroups, ...[groupId]])]
                }, () => {
                    if (this.contains)
                        console.log("ADD_ON_GROUP_NOT_EXIST_THEN_ADDED_RADIO:" + JSON.stringify(this.state.selectedAddOns))
                })
                // }
            }
        } else {
            console.log("NOTHING")
        }
    }

    getTotalPrice = (isQuntitiyIncluded) => {
        let productPrice = parseInt(this.state.product.cost)
        let addOnPrice = 0

        this.state.selectedAddOnsGroups.forEach(element => {
            let addOns = this.state.selectedAddOns[element]
            addOns.forEach(item => {
                addOnPrice += parseInt(item.price)
            });
        });

        if (isQuntitiyIncluded) {
            return (this.state.quantity * (productPrice + addOnPrice))

        } else {
            return (productPrice + addOnPrice)
        }


    }


    showBasketAlert = () => {
        Alert.alert(
            strings.title_basket,
            strings.cart_dialog,
            [
                //   {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                {
                    text: strings.cancel,
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {
                    text: strings.ok,
                    onPress: () => this.clearBasket()
                },
            ],
            { cancelable: false },
        );
    }

    clearBasket = () => {
        this.props.addItemInModifyBasketAction(null)
        this.initializeBasket()
        NavigationService.goBack()
    }

    initializeBasket = () => {
        const restDetail = this.state.restDetail
        const restId = this.state.restDetail.id
        const totalQuantity = this.state.quantity
        const totalPrice = this.getTotalPrice(true)
        const totalCgst = totalQuantity * parseInt(restDetail.cgst)
        const totalSgst = totalQuantity * parseInt(restDetail.sgst)
        let product = this.state.product

        product = {
            ...product,
            quantity: this.state.quantity,
            productPrice: this.getTotalPrice(true),
            unitPrice:this.getTotalPrice(false),
            selectedAddOns: this.state.selectedAddOns,
            selectedAddOnsGroups: this.state.selectedAddOnsGroups
        }

        const basket = {
            restDetail: restDetail,
            restId: restId,
            totalQuantity: totalQuantity,
            totalPrice: totalPrice,
            totalCgst: totalCgst,
            totalSgst: totalSgst,
            products: [product]
        }

        this.props.addItemInModifyBasketAction(basket)
        //AsyncStorage.setItem(Constants.STORAGE_KEY.basketData, JSON.stringify(basket));
    }

    addItemInAlreadyFilledBasket = () => {
        const restDetail = this.state.restDetail
        const restId = this.state.restDetail.id
        let totalQuantity = 0
        // = this.props.basketData.totalQuantity + 1
        let totalPrice = 0
        // = this.getTotalPrice(false) + this.props.basketData.totalPrice
        let totalCgst = 0
        // = totalQuantity * parseInt(restDetail.cgst)
        let totalSgst = 0
        // = totalQuantity * parseInt(restDetail.sgst)
        let product = this.state.product

        product = {
            ...product,
            quantity: this.state.quantity,
            productPrice: this.getTotalPrice(true),
            unitPrice:this.getTotalPrice(false),
            selectedAddOns: this.state.selectedAddOns,
            selectedAddOnsGroups: this.state.selectedAddOnsGroups
        }



        var alreadyAddedProductsInBasket = [...this.props.basketData.products]; // make a separate copy of the array
        let productIndex = -1
        alreadyAddedProductsInBasket.forEach((element, index) => {
            if (element.id === product.id) {
                productIndex = index
            }else{
                totalQuantity += element.quantity
                totalPrice += element.productPrice
            }
        });

        totalQuantity = totalQuantity + this.state.quantity
        totalPrice = totalPrice + this.getTotalPrice(true)
        totalCgst = totalQuantity * parseInt(restDetail.cgst)
        totalSgst = totalQuantity * parseInt(restDetail.sgst)

        let products = []
        if (productIndex != -1) {
            // alreadyAddedProductsInBasket.splice(productIndex, 1);
            alreadyAddedProductsInBasket[productIndex] = product
            products = alreadyAddedProductsInBasket
        }else{
            if (alreadyAddedProductsInBasket && alreadyAddedProductsInBasket.length > 0) {
                products = [...alreadyAddedProductsInBasket, ...[product]]
            } else {
                products = [product]
            }
        }

        const basket = {
            restDetail: restDetail,
            restId: restId,
            totalQuantity: totalQuantity,
            totalPrice: totalPrice,
            totalCgst: totalCgst,
            totalSgst: totalSgst,
            products: products
        }

        this.props.addItemInModifyBasketAction(basket)
        //AsyncStorage.setItem(Constants.STORAGE_KEY.basketData, JSON.stringify(basket));
    }

    onAddToBasketClick = () => {
        if (!this.props.basketData) {
            this.initializeBasket()
            NavigationService.goBack()
            // initializeBasket(this.props.basketData,addItemInModifyBasketAction,restDetail,product)

        } else {
            if (this.props.basketData.restId != this.state.restDetail.id) {
                this.showBasketAlert()
            } else {
                this.addItemInAlreadyFilledBasket()
                NavigationService.goBack()
            }
        }
    }

    onPlaceOrderClicked = () => {
        if (!this.props.basketData) {
            this.initializeBasket()
            NavigationService.navigate("ModifyBasket")
            // initializeBasket(this.props.basketData,addItemInBasketAction,restDetail,product)

        } else {
            if (this.props.basketData.restId != this.state.restDetail.id) {
                this.showBasketAlert()
            } else {
                this.addItemInAlreadyFilledBasket()
                NavigationService.navigate("ModifyBasket")
            }
        }
    }

    onBackClick = () => {
        NavigationService.goBack()
    }

    renderAddOnHeader() {
        if (this.state.product === null) {
            return
        }

        const list = this.state.product.addons.map((item, key) => (
            // return (
            <View style={styles.headerContainer} key={key}
                value={item}>
                <View>
                    <TextBold title={item.name} textStyle={styles.headerText} />
                </View>
                <View>
                    <ScrollView>
                        {this.renderAddOnItem(item.data)}
                    </ScrollView>
                </View>

            </View>
            // )
        ))

        return (
            <View style={{ paddingHorizontal: 20, marginBottom: 50 }}>
                {list}
            </View>
        )

    }

    renderAddOnType(item) {
        if (item.type === 'checkbox') {
            return (
                <CheckBox
                    style={{ flex: 1, paddingVertical: 10, }}
                    checkBoxColor={Constants.color.primary}
                    onClick={() => {
                        this.addAddOnInList(item)
                    }}
                    isChecked={
                        (this.state.selectedAddOns
                            && this.state.selectedAddOns[item.id]
                            && (this.contains(this.state.selectedAddOns[item.id], item) != -1)
                        ) ? true : false
                    }
                    rightText={item.item_name}
                    rightTextStyle={styles.rightTextStyle}
                />
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={1} style={{ flexDirection: 'row', paddingVertical: 10, flex: 1 }} onPress={() => this.addAddOnInList(item)}>
                    {/* <View > */}
                    <RadioButton
                        size={12}
                        outerColor={Constants.color.primary}
                        innerColor={Constants.color.primary}
                        onPress={() => this.addAddOnInList(item)}
                        isSelected={
                            (this.state.selectedAddOns
                                && this.state.selectedAddOns[item.id]
                                && (this.contains(this.state.selectedAddOns[item.id], item) != -1)
                            ) ? true : false
                        }
                    />
                    <TextRegular title={item.item_name} textStyle={[styles.rightTextStyle, { paddingHorizontal: 10 }]} />
                    {/* </View> */}

                </TouchableOpacity>

            )
        }

    }



    renderAddOnItem(items) {
        const list = items.map((item, key) => (
            <View key={key} value={item} style={{ flexDirection: 'row' }}>
                {this.renderAddOnType(item)}

                <View style={{ justifyContent: 'center' }}>
                    <TextRegular title={Constants.currency.dollar + item.price} textStyle={styles.rightTextStyle} />
                </View>
            </View>


        ))

        return (
            <View>
                {list}
            </View>
        )
    }

    renderBackButton() {
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }

    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbarWithNoMenu}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '90%'
                }
                }>
                    {this.renderBackButton()}
                    <TextBold title={this.state.menuName} textStyle={GlobalStyle.toolbarWithNoMenuText} />

                </View>
            </View>

        )
    }

    renderMenuInfoView() {
        return (
            <View>
                <View style={styles.menuInfo}>
                    <TextBold title={this.state.product ? this.state.product.name : ""} textStyle={styles.menuInfoText} />
                    <View style={styles.menuInfoView}>
                        <Image source={(this.state.product && this.state.product.image && this.state.product.image.location && this.state.product.image.location != "") ? { uri: this.state.product.image.location } : Images.no_restaurant} style={styles.menuInfoImage}></Image>

                    </View>
                </View>
                <View style={GlobalStyle.horizontalDivider} />
            </View>
        )
    }

    renderQuantityView() {
        const amount = (this.state.product && this.state.product.cost) ? this.state.product.cost : ""
        return (
            <View>
                <View style={[styles.menuInfo, { paddingVertical: 20 }]}>
                    <TextRegular title={strings.quantity} textStyle={[styles.quantityText, { flex: 4 }]} />
                    <View style={[styles.quantityAddItem, { flex: 4 }]}>
                        {/* <Image source={Images.no_restaurant} style={styles.menuInfoImage}></Image> */}
                        <CustomAddItem
                            quantity={this.state.quantity}
                            addItem={this.changeQuantity('add')}
                            removeItem={this.changeQuantity('remove')}
                        />
                    </View>
                    <View style={[styles.quantityPrice, { flex: 4 }]}>
                        <TextBold title={((this.state.quantity > 1) ? (this.state.quantity + ' X ') : '') + Constants.currency.dollar + amount} textStyle={styles.quantityPriceText} />
                    </View>
                </View>
                <View style={GlobalStyle.horizontalDivider} />
            </View>
        )
    }

    renderBottomView() {
        return (
            <View>
                <View style={styles.bottomView}>
                    <TouchableOpacity style={styles.bottomTouchable} onPress={() => this.onPlaceOrderClicked()}>
                        <TextRegular title={strings.ordernow} textStyle={styles.bottomButtonText} />
                    </TouchableOpacity>
                    <View style={{ backgroundColor: Constants.color.seperator_color, width: 1, height: '60%', alignSelf: 'center' }} />
                    <TouchableOpacity style={styles.bottomTouchable} onPress={() => this.onAddToBasketClick()}>
                        <TextRegular title={strings.addtobasket} textStyle={styles.bottomButtonText} />
                    </TouchableOpacity>
                </View>
            </View>

        )
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <SafeAreaView style={GlobalStyle.defaultSafeAreaStyle} />
                {this.renderToolbar()}
                <ScrollView >
                    {this.renderMenuInfoView()}
                    {this.renderQuantityView()}
                    <View style={styles.addOnView}>
                        <TextBold title={strings.add_on} textStyle={styles.addOnText} />
                    </View>
                    {this.renderAddOnHeader()}
                </ScrollView>
                {this.renderBottomView()}
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>

            </View>
        );
    };
}

function mapStateToProps(state) {
    // console.log("ADD_ONS_STATE:", JSON.stringify(state))
    return {
        basketData: state.modifyBasketData,

    }
}

export default connect(mapStateToProps, { addItemInModifyBasketAction })(AddOnsModify)