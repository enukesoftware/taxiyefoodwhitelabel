import { Platform, StyleSheet } from 'react-native';
import { Fonts } from '../../utils/fonts'
import { Constants } from '../../utils'


export default StyleSheet.create({

    menuInfo: {
        flexDirection: 'row',
        paddingVertical: 10,
        paddingHorizontal: 20,
        width: '100%',
        alignItems: 'center'
    },
    menuInfoText: {
        color: Constants.color.primary,
        flex: 9,
        fontSize: Constants.fontSize.NormalXX
    },
    menuInfoView: {
        flex: 3,
        alignItems: 'flex-end',
    },
    menuInfoImage: {
        width: 60,
        height: 60,
        borderRadius: 30,
        borderWidth: 1,
        borderColor: Constants.color.primary,
        alignSelf: 'flex-end',
    },
    quantityText: {
        color: Constants.color.dark_gray,
        fontSize: Constants.fontSize.NormalX
    },
    quantityAddItem: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    quantityPrice: {
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    quantityPriceText: {
        color: Constants.color.fontBlack,
        alignSelf: 'flex-end',
        fontSize: Constants.fontSize.NormalX
    },
    addOnView: {
        paddingVertical: 10,
        paddingHorizontal: 20,
        backgroundColor: Constants.color.bg_light,
        marginBottom:10,
    },
    addOnText: {
        color: Constants.color.drakgray,
        fontSize: Constants.fontSize.NormalX
    },
    bottomView: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: Constants.color.primary,
        flexDirection: 'row',
    },
    bottomTouchable:{
        alignItems: 'center',
        justifyContent: 'center',
        padding:15,
        flex:6,
    },
    bottomButtonText: {
        color:Constants.color.white,
        fontSize:Constants.fontSize.NormalXX
    },
    rightTextStyle: {
        fontFamily: Fonts.Regular,
        fontSize: Constants.fontSize.NormalX,
    },
    headerContainer: {
        paddingTop: 10,
        paddingHorizontal: 15,
        marginBottom: 10,
        borderRadius: 5,
        backgroundColor: 'white',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.2,
        elevation: 5
    },
    headerText: {
        fontSize: Constants.fontSize.NormalXX,
        opacity: 0.8,
        marginBottom: 5,
    }
})