import React, { Component } from 'react'
import { Platform, Text, View, TouchableOpacity, ImageBackground, Image, ActivityIndicator } from 'react-native';
import styles from './styles'
import { SafeAreaView } from 'react-navigation';
import { connect } from 'react-redux'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import { ScrollView } from 'react-native-gesture-handler';
import { dealsApi } from '../../services/APIService'
import { getCuisinesList } from '../../services/APIService'
import { userDataAction } from '../../redux/actions'


class Deals extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            header: null,
        }
    };
    componentDidMount() {
        this.callDealsApi()
    }





    constructor() {
        super()
        this.state = {
            user_id: '',
            discountList: [],
            discountListOld: [
                {
                    discount: "20.00%",
                    couponCode: "Fresh20",
                    restro: "Fresh Menu"
                },
                {
                    discount: "$50.00",
                    couponCode: "ODI",
                    restro: "Cake bake"
                },
                {
                    discount: "$50.00",
                    couponCode: "ODI",
                    restro: "Cake bake"
                },
                {
                    discount: "$50.00",
                    couponCode: "ODI",
                    restro: "Cake bake"
                },
                {
                    discount: "$50.00",
                    couponCode: "ODI",
                    restro: "Cake bake"
                },
            ]
        }
    }

    getCouponValue(cvalue, type) {
        let value
        switch (type) {
            case "0":
                value = strings.currency_symbol+cvalue
                break;
            case "1":
                value = cvalue+"%"
        }
        return value
    }

    discountlist = () => {
        return this.state.discountList.map((item, index) =>
            (
                // Main Card View
                <View key={index} style={styles.viewDiscountCard}>

                    {/* Main Touchable Opacity */}
                    <TouchableOpacity style={styles.touchDiscountCard}
                    onPress={()=>{
                        this.props.navigation.navigate("RestaurantMenu", { item: item.restaurants})
                    }}
                    >

                        {/* Background Image for View */}
                        <ImageBackground
                            style={styles.imageBackground}
                            source={Images.info_background}>

                            {/* Left Colored View  */}
                            <View style={styles.viewPrimary}>
                                {/*Texts inside Left View  */}
                                <TextBold title={strings.flat + "  " + this.getCouponValue(item.coupon_value, item.type)} textStyle={styles.textFlat} />
                                {<TextBold title={strings.offer} textStyle={styles.textFlat} />}
                                <TextRegular title={strings.couponcode} textStyle={styles.textThin} />
                                <TextRegular title={item.coupon_code} textStyle={[styles.textThin, { paddingTop: 0 }]} />
                            </View>

                            {/* Right transparent View  */}
                            <View style={styles.viewSecondary}>
                                {/* Small Arrow View  */}
                                <View style={{ width: '10%' }}>
                                    <Image style={styles.imgArrow} source={Images.ic_play}></Image>
                                </View>

                                {/* Logo and Name of restro View  */}
                                <View style={styles.viewChild}>
                                    <View style={styles.viewImgLogo}>
                                    <Image style={styles.imgLogo} source={
                                        item.image==null?
                                        Images.no_restaurant:Images.resto_logo
                                    }></Image>
                                    </View>
                                    <TextBold title={item.restaurants.name} textStyle={{ color: Constants.color.white }}></TextBold>
                                </View>
                            </View>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>
            )
        )

    }

    onBackClick = () => {
        const { navigation } = this.props;
        navigation.goBack();
    }

    renderBackButton() {
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }
    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '90%'
                }}>
                    {this.renderBackButton()}
                    <TextBold title={strings.deals} textStyle={styles.textTitle} />

                </View>
            </View>

        )
    }
    renderProgressBar() {
        if (this.state.isLoading) {
            return (
                <View style={GlobalStyle.activityIndicatorView}>
                    <View style={GlobalStyle.activityIndicatorWrapper}>
                        <ActivityIndicator
                            size={"large"}
                            color={Constants.color.primary}
                            animating={true} />
                    </View>
                </View>
            )
        } else {
            return
        }
    }

    callDealsApi() {
        if(!this.props.internet){
            alert(strings.message_lno_internet)
            return
        }
        this.setState({
            isLoading: true,
        })
        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.deals

        dealsApi(url).then(res => {
            // this.setState({
            //     isLoading: false,
            // })

            if (res && res.success) {
                console.log("DEALS_RESPONSE:" + JSON.stringify(res))
                this.setState({
                    isLoading: false,
                    discountList: res.data
                })

            } else {
                this.setState({
                    isLoading: false,
                }, () => {
                    if (res && res.error) {
                        alert(res.error)
                    }
                })
            }

        }).catch(err => {
            this.setState({
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    alert(JSON.stringify(err));
                }
            }, 100);
        });

    }
    render() {
        return (
            <View style={styles.container}>
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
                <View style={{ flex: 1 }}>
                    {this.renderToolbar()}
                    <ScrollView style={styles.scrollView}>
                        {/* List Of Discounts */}
                        {this.discountlist()}
                    </ScrollView>

                </View>
                {this.renderProgressBar()}
            </View>
        );
    };
}

function mapStateToProps(state) {
    //console.log("BASKET_STATE:" + JSON.stringify(state.basketData))
    return {
        internet:state.internet,
    }
}

export default connect(mapStateToProps, {})(Deals)