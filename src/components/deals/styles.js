import { StyleSheet, Platform } from 'react-native';
import { Constants } from '../../utils'


export default StyleSheet.create({
    container: {
        flex: 1,
        // width:'100%',
    },

    scrollView: {
        marginBottom: 5,
    },
    textTitle: {
        fontSize: 20,
        color: Constants.color.white,
        marginLeft: 15,
    },
    viewDiscountCard: {
        paddingTop: 10, paddingHorizontal: 10, height: 130
    },
    touchDiscountCard: {
        borderBottomLeftRadius: 10,
        overflow: 'hidden',
        borderTopLeftRadius: 10,
        flexDirection: 'row',
        height: '100%', width: '100%'
    },
    imageBackground: {
        width: '100%', height: '100%',
        flexDirection: 'row',
    },
    viewPrimary: {
        width: '40%',
        height: '100%',
        padding: 15,
        justifyContent: "center",
        backgroundColor: Constants.color.primary
    },
    textFlat: {
        fontSize: 16,
        color: Constants.color.white
    },
    textThin: {
        fontSize: 12,
        paddingTop: 15,
        color: Constants.color.white
    },
    viewSecondary: {
        width: '60%', height: '100%',
        flexDirection: "row",
        alignItems: 'center',
    },
    imgArrow: { width: 20, height: 35 },
    viewChild: { width: '90%', alignItems: 'flex-end', padding: 15 },
    viewImgLogo: {
        width: 40,
        height: 40,
        borderRadius:20,
        overflow: "hidden",
        backgroundColor: 'transparent'
    },
    imgLogo: {
        resizeMode:'contain',
        width:'100%',
        height:'100%',
    },


})