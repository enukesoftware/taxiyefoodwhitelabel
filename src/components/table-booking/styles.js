import { Platform, StyleSheet } from 'react-native';
import { Fonts } from '../../utils/fonts'
import { Constants } from '../../utils'


export default StyleSheet.create({

    toolbar: {
        height: (Platform.OS === Constants.PLATFORM.ios) ? Constants.TOOLBAR_HEIGHT.ios : Constants.TOOLBAR_HEIGHT.android,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Constants.color.primary,
        shadowOffset: { width: 10, height: 2 },
        shadowOpacity: 0.2,
        elevation: 15
    },
    scrollView: {
        marginBottom: 5,
    },
    textTitle: {
        fontSize: 20,
        color: Constants.color.white,
        marginLeft: 15,
    },
    loginForm: {
        padding: 30,
        width: '100%',
        flex: 8
    },
    textInput: {
        fontSize: 16,
        height: 40,
        fontFamily: Fonts.Regular,
        width: '100%',
        color: Constants.color.black
    },
    inputLayout: {
        width: '100%',
        marginHorizontal: 10
    },
    iconLeft: {
        height: 25,
        width: 25,
        margin: 5
    },
    viewInput: {
        paddingTop: 5,
        flexDirection: "row",
        alignItems: 'flex-end',
    },
    imgBack: {
        width: '100%',
        height: '100%'
    },
    touchOpacity: {
        backgroundColor: Constants.color.primary,
        justifyContent: "center",
        alignItems: "center",
        width: '100%',
        padding: 15,
        borderRadius: 5
    },
    textButtonAdd: {
        color: Constants.color.white,
        fontSize: 16
    },
    viewButton: {
        alignItems: 'center',
        justifyContent: "center",
        paddingTop: 20,
    },
    touchItemDialoge: {
        flexDirection: 'row',
        width: '100%',
        alignItems: "center"
    },
    viewRadio: {
        padding: 5,
        width: '20%'
    },
    activityIndicatorView: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
    },
    activityIndicatorWrapper: {
        backgroundColor: Constants.color.gray,
        height: 100,
        width: 100,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        opacity: 0.8,
    },

    titleStyle: {
        textAlign: 'center',
        color: Constants.color.black,
        fontFamily: Fonts.Bold,
    },


})
