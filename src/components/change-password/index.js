import React, { Component } from 'react'
import {Platform, TextInput, SafeAreaView, TouchableHighlight, Image, View, TouchableOpacity, Text,ActivityIndicator } from 'react-native';
import styles from './styles'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import { TextInputLayout } from 'rn-textinputlayout';
import { ScrollView } from 'react-native-gesture-handler';
import { connect } from 'react-redux'
import { changePasswordApi } from '../../services/APIService'
import NavigationService from '../../services/NavigationServices'

class ChangePassword extends Component {

    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            old_password: "",
            new_password: "",
            confirm_password: "",
        }

       
    }

    _verifyAndChange = () => {
        if (this.state.old_password == "") {
            alert(strings.old_password_error)
            return
        }
        if (this.state.old_password.length < 6) {
            alert(strings.valid_password_error)
            return
        }
        if (this.state.new_password == "") {
            alert(strings.new_password_error)
            return
        }
        if (this.state.new_password.length < 6) {
            alert(strings.valid_password_error)
            return
        }
        if (this.state.confirm_password == "") {
            alert(strings.confirm_pass_error)
            return
        }
        if (this.state.new_password != this.state.confirm_password) {
            alert(strings.conform_password_error)
            return
        }

        this.callChangePasswordApi()

        //here u change password
    }

    callChangePasswordApi() {
        if (!this.props.internet) {
            alert(strings.message_lno_internet)
            return
        }

        const param = {
            id:this.props.userData.id,
            old_password: this.state.old_password,
            new_password: this.state.new_password,
        }

        this.setState({
            isLoading: true,
        })

        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.CHANGE_PASSWORD

        changePasswordApi(url, param).then(res => {

            console.log("CHANGE_PWD_API_RES:" + JSON.stringify(res))
            // this.setState({
            //     isLoading: false,
            // })

            if (res && res.success) {
                this.setState({
                    isLoading: false,
                }, () => {
                    NavigationService.goBack()
                    // if (res.data) {
                    //     AsyncStorage.setItem(Constants.STORAGE_KEY.userData, JSON.stringify(res.data));
                    //     AsyncStorage.setItem(Constants.STORAGE_KEY.isLogin, JSON.stringify({ isLogin: true }));
                    //     this.props.userDataAction(res.data)
                    //     this.props.isLoginAction(true)
                    //     callUpdateDeviceTokenApi()
                    //     NavigationService.navigate(previousPage)
                    // }

                })
            } else {
                this.setState({
                    isLoading: false,
                })
                if (res && res.message) {
                    alert(res.message)
                }else if(res && res.error){
                    alert(res.error)
                }
            }

        }).catch(err => {
            this.setState({
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    alert(JSON.stringify(err));
                }
            }, 100);
        });
    }

    onBackClick = () => {
        const { navigation } = this.props;
        navigation.goBack();
    }



    renderBackButton() {
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }


    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '90%'
                }
                }>
                    {this.renderBackButton()}
                    <TextBold title={strings.side_menu_change_paasword} textStyle={styles.textTitle}/>

                </View>
                {/* <View style={{ flexDirection: 'row',  width: '10%' }}>
                    <TouchableOpacity activeOpacity={0.8}
                    >
                        <Image source={Images.ic_close_white}
                            style={{ marginRight: 15, height: 25, width: 25 }}
                        ></Image>
                    </TouchableOpacity>
                </View> */}
            </View>

        )
    }

    renderProgressBar() {
        if (this.state.isLoading) {
            return (
                <View style={GlobalStyle.activityIndicatorView}>
                    <View style={GlobalStyle.activityIndicatorWrapper}>
                        <ActivityIndicator
                            size={"large"}
                            color={Constants.color.primary}
                            animating={true} />
                    </View>
                </View>

            )
        } else {
            return
        }

    }

    render() {

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Constants.color.primary }}>
                <View style={{ flex: 1, backgroundColor: Constants.color.white }} >
                    {this.renderToolbar()}

                    <View style={styles.loginForm}>
                        <ScrollView contentInsetAdjustmentBehavior="automatic"
                         keyboardShouldPersistTaps='always'
                         style={{}}>
                            {/* Old Password*/}
                            <View style={styles.viewInput}>
                                <Image resizeMode='contain' source={Images.ic_change_password}
                                 style={styles.iconLeft}></Image>
                                <TextInputLayout
                                    style={styles.inputLayout}
                                    focusColor={Constants.color.black}>
                                    <TextInput
                                        secureTextEntry={true}
                                        style={styles.textInput}
                                        onChangeText={(text) => {
                                            this.setState({
                                                old_password: text
                                            });
                                        }}
                                        placeholder={strings.hint_oldPassword} />
                                </TextInputLayout>
                            </View>

                            {/* New Password*/}
                            <View style={styles.viewInput}>
                                <Image resizeMode='contain' source={Images.ic_password}
                                 style={styles.iconLeft}></Image>
                                <TextInputLayout
                                    style={styles.inputLayout}
                                    focusColor={Constants.color.black}>
                                    <TextInput
                                        secureTextEntry={true}
                                        onChangeText={(text) => {
                                            this.setState({
                                                new_password: text
                                            });
                                        }}
                                        style={styles.textInput}
                                        placeholder={strings.hint_newPassword} />
                                </TextInputLayout>
                            </View>

                            {/*Confirm Password*/}
                            <View style={styles.viewInput}>
                                <Image resizeMode='contain' source={Images.ic_password} style={styles.iconLeft}></Image>
                                <TextInputLayout
                                    style={styles.inputLayout}
                                    focusColor={Constants.color.black}>
                                    <TextInput
                                        secureTextEntry={true}
                                        onChangeText={(text) => {
                                            this.setState({
                                                confirm_password: text
                                            });
                                        }}
                                        style={styles.textInput}
                                        placeholder={strings.hint_confirmpassword} />
                                </TextInputLayout>
                            </View>

                            {/*Button Register */}
                            <View style={styles.viewButton}>
                                <TouchableOpacity
                                    onPress={
                                        ()=>this._verifyAndChange()
                                    }
                                    style={styles.touchOpacity}>
                                    <TextBold title={strings.side_menu_change_paasword} textStyle={styles.textButtonAdd}/>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                    {this.renderProgressBar()}
                </View>
            </SafeAreaView>
        );
    }
}

function mapStateToProps(state) {
    return {
        internet: state.internet,
        userData:state.userData,
    }
}

export default connect(mapStateToProps, {})(ChangePassword)






