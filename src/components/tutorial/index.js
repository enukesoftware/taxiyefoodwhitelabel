import React, { Component } from 'react'
import { Platform, StyleSheet, StatusBar, View, Text, Image, TouchableOpacity, SafeAreaView, AsyncStorage } from 'react-native';
import styles from './styles'
import { connect } from 'react-redux'
import { Constants, strings, Images } from '../../utils'
import { IndicatorViewPager } from 'rn-viewpager';
import { TextRegular } from '../custom/text'
import { Step1, Step2, Step3, Step4 } from './steps'
import NavigationService from '../../services/NavigationServices'

class Tutorial extends Component {


    static navigationOptions = ({ navigation }) => ({
        header: null
    });


    constructor() {
        super()
        this.state = {
            pageNumber: 0
        }
    }

    changePage() {
        if (this.state.pageNumber < 3) {
            this.setState({
                pageNumber: this.state.pageNumber + 1,
            }, () => {
                this.pager.setPage(this.state.pageNumber)
            })

        } else {
            AsyncStorage.setItem(Constants.STORAGE_KEY.isVisitedSteps, 'true');
            NavigationService.navigate('SelectLocality', { NavigateFrom: "TutorialScreen" })
        }
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: Constants.color.white }}>
                <SafeAreaView style={{ backgroundColor: Constants.color.white }}></SafeAreaView>
                <StatusBar
                    barStyle="dark-content"
                    backgroundColor={Constants.color.white} />
                {/* <View style={{ flex: 1 }}> */}
                {/* <SafeAreaView style={{ backgroundColor: Constants.color.primaryDark }} /> */}

                <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
                    {/* <Text>Hello Tutorial</Text>
                    <Button title='Click' onPress={() => this.props.navigation.navigate('MyComponent')}></Button> */}
                    <IndicatorViewPager
                        ref={pager => this.pager = pager}
                        scrollEnabled={false}
                        style={{ flex: 1 }}
                    // indicator={this._renderDotIndicator()}
                    >
                        <View>
                            <Step1 />
                        </View>
                        <View >
                            <Step2 />
                        </View>
                        <View >
                            <Step3 />
                        </View>
                        <View >
                            <Step4 />
                        </View>
                    </IndicatorViewPager>

                    <TouchableOpacity activeOpacity={0.9} onPress={() => this.changePage()} style={{ backgroundColor: Constants.color.primary, height: 60, alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ flexDirection: 'row' }}>
                            <TextRegular title={strings.continue} textStyle={{ fontSize: 18, color: 'black' }}></TextRegular>
                            <Image style={styles.nextArrowStyle} source={Images.ic_next_arrow}></Image>
                        </View>
                    </TouchableOpacity>
                    <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
                </View>
                 {/* </View> */ }
                 
            </View>
               
            

        );
    };
}

// export default Tutorial

function mapStateToProps(state) {
    return {
        isInternetConnected: state.internet,
    }
}

// export default MyComponent

export default connect(mapStateToProps)(Tutorial)

