import React, { Component } from 'react'
import { Platform, SafeAreaView, NativeModules, Image, View, TouchableOpacity, ActivityIndicator,Alert } from 'react-native';
import styles from './styles'
import { Constants, strings, Images, Fonts, GlobalStyle } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import { ScrollView } from 'react-native-gesture-handler';
import RadioButton from 'react-native-radio-button'
import { addressApi, deleteAddressApi } from '../../services/APIService'
import { connect } from 'react-redux'
import { userDataAction, isLoginAction } from '../../redux/actions'
import { NavigationEvents } from "react-navigation"

let KEY_ORDER;
let cameFrom = 2;


class SelectDeliveryAddress extends Component {

    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            isLoading: false,
            addresslist: [],
            renderList: false,
            selectedIndex: -1
        }
    }

    componentDidMount() {
        const { navigation } = this.props;
        const navigateFrom = navigation.getParam('NavigateFrom', '');

        //console.log('addessJson:'+navigateFrom)
        if (navigateFrom == 'Basket' || navigateFrom == 'ModifyBasket') {
            cameFrom = 1;
            let key = navigation.getParam('keys', '');
            // console.log('addessJson:' + JSON.stringify(key))
            KEY_ORDER = key;
        }else{
            cameFrom = 2;
        }

        if (this.props.userData) {
            const userData = this.props.userData
            userId = (userData.id) ? userData.id : ""
            this.setState({
                user_id: userId
            })
            this.callLoadAddressApi(userId)
        }


    }

    onBackClick = () => {
        const { navigation } = this.props;
        navigation.goBack();
    }



    renderBackButton() {
        if (Platform.OS === Constants.PLATFORM.ios) {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_ios}
                        style={{ marginLeft: 5, height: 22, width: 22, }}
                    ></Image>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity activeOpacity={0.8}
                    onPress={() => this.onBackClick()}>
                    <Image source={Images.ic_back_android}
                        style={{ marginLeft: 15, height: 35, width: 25, }}
                    ></Image>
                </TouchableOpacity>
            )
        }
    }

    renderToolbar() {
        return (
            <View style={GlobalStyle.toolbar}>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '90%'
                }
                }>
                    {this.renderBackButton()}
                    <TextBold title={strings.drop_location_button} textStyle={styles.textTitle} />
                </View>
            </View>

        )
    }

    _selectSelected = (index) => {
        if (index == this.state.selectedIndex)
            return true
        else
            return false
    }

    CalculateAddress(address_json) {
        let hn = address_json.house_no
        let st = address_json.street
        let ar = address_json.area_name
        let ci = address_json.city
        let stA = address_json.state_id
        let address = ""
        if (hn != null && hn != "") {
            address = address + hn + ', '
        }
        if (st != null && st != "") {
            address = address + st + ', '
        }
        if (ar != null && ar != "") {
            address = address + ar + ', '
        }
        if (ci != null && ci != "") {
            address = address + ci + ', '
        }
        if (stA != null && stA != "") {
            address = address + stA
        }

        return address
    }

    tabClicked(index, id) {
        // if(this.state.addresslist.length>=index+1){

        // }
        this.setState({
            selectedIndex: index,
        })
        KEY_ORDER.order["delivery_address_id"] = id
        console.log('KEY_ORDER:' + JSON.stringify(KEY_ORDER))
        if (KEY_ORDER.order["delivery_address_id"] != null && KEY_ORDER.order["delivery_address_id"] != "") {
            this.props.navigation.navigate('Checkout', {
                NavigateFrom: this.props.navigation.getParam('NavigateFrom', ''),
                KEY_ORDER: KEY_ORDER
            })
        }
    }

    showDeleteAlert = (id) => {
        Alert.alert(
          strings.delete_dialog_title,
          strings.delete_dialog_message,
          [
            {
              text: strings.cancel,
              onPress: () => console.log('Cancel Pressed'),
              style: 'cancel',
            },
            {
              text: strings.ok,
              onPress: () => this.onDeleteAlertClick(id)
            },
          ],
          { cancelable: false },
        );
      }

      onDeleteAlertClick = (id) => {
        // this.deleteSelectedAddress();
        this.setState({
            isLoading: true,
        }, () => {
            this.callDeleteAddressApi(id)
        })
        
      }


      deleteSelectedAddress = (id) => {
        let addresslist = this.state.addresslist;
        let newAddresslist = addresslist.filter((item) => {
            if (item.id != id) {
                return item;
            }
        })


        if (newAddresslist && newAddresslist.length > 0) {
            this.setState({
                addresslist:newAddresslist,
                isLoading: false,
            })
        }else{
            this.setState({
                addresslist:[],
                isLoading: false,
            })
        }
      }

      callDeleteAddressApi(id) {
        if(!this.props.internet){
            this.setState({
                isLoading:false
            },()=>{
                alert(strings.message_lno_internet)
            })
            
            return
        }
        const param = {
            user_id: this.state.user_id,
            address_id: id,
        }
        

        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.deleteAddress

        deleteAddressApi(url, param).then(res => {
            // this.setState({
            //     isLoading: false,
            // })

            if (res && res.success) {
                this.deleteSelectedAddress(id)
                // alert(res.message);

            } else {
                this.setState({
                    isLoading: false,
                }, () => {
                    if (res && res.error) {
                        alert(res.error)
                    }
                })
            }

        }).catch(err => {
            this.setState({
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    alert(JSON.stringify(err));
                }
            }, 100);
        });

    }

    _addresslist = () => {
        return this.state.addresslist.map((item, index) =>
            (
                item.address_json === '' || item.address_json === null ?
                    null :
                    <View key={index}>
                        <TouchableOpacity style={styles.mainCard}
                            onPress={() => cameFrom == 1 ? this.tabClicked(index, item.id) : null} >
                            {/* View For Image and Items*/}
                            <View style={{ flexDirection: 'row' }}>
                                {/* View For Image */}
                                <View style={styles.viewRadio}>
                                    {
                                        cameFrom == 1 ?
                                            <RadioButton
                                                size={12}
                                                outerColor={Constants.color.primary}
                                                innerColor={Constants.color.primary}
                                                onPress={() => this.tabClicked(index, item.id)}
                                                isSelected={this._selectSelected(index)}
                                            />
                                            : null
                                    }
                                </View>
                                {/* View For Items */}
                                <View style={styles.viewItems}>
                                    <View style={styles.viewInputRow}>
                                        <Image style={{ height: 20, width: 20, marginHorizontal: 5 }} resizeMode='contain'
                                            source={Images.ic_house} />
                                        <TextRegular title={this.CalculateAddress(item.address_json)} textStyle={styles.textItems} />
                                    </View>
                                    <View style={styles.viewInputRow}>
                                        <View style={{ width: '60%', flexDirection: 'row' }}>
                                            <Image style={{ height: 20, width: 20, marginHorizontal: 5 }} resizeMode='contain'
                                                source={Images.ic_address} />
                                            <TextRegular title={item.address_json.city} textStyle={styles.textItems} />
                                        </View>

                                        <View style={{ width: '40%', flexDirection: 'row', }}>
                                            <Image style={{ height: 20, width: 20, marginHorizontal: 5 }} resizeMode='contain'
                                                source={Images.ic_state} />
                                            <TextRegular title={item.address_json.state_id} textStyle={styles.textItems} />
                                        </View>

                                    </View>

                                </View>
                                
                                <TouchableOpacity style={[styles.viewRadio, { alignItems: 'flex-end' }]}
                                    onPress={() => {this.showDeleteAlert(item.id) }}>
                                    <Image style={{ height: 30, width: 30, paddingHorizontal: 5, alignSelf: 'flex-end' }} resizeMode='contain'
                                        source={Images.ic_delete} />
                                </TouchableOpacity>
                            </View>

                        </TouchableOpacity>
                    </View>

            )
        )


    }

    renderProgressBar() {
        if (this.state.isLoading) {
            return (
                <View style={GlobalStyle.activityIndicatorView}>
                    <View style={GlobalStyle.activityIndicatorWrapper}>
                        <ActivityIndicator
                            size={"large"}
                            color={Constants.color.primary}
                            animating={true} />
                    </View>
                </View>
            )
        } else {
            return
        }

    }
    callLoadAddressApi(userId) {
        if (!this.props.internet) {
            alert(strings.message_lno_internet)
            return
        }
        const param = {
            user_id: userId,
            type: 'list',
        }
        this.setState({
            isLoading: true,
        })
        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.address

        addressApi(url, param).then(res => {

            if (res && res.success) {
                console.log("GET_ADDRESS_RESPONSE:" + JSON.stringify(res))
                this.setState({
                    isLoading: false,
                    selectedIndex: -1,
                    addresslist: res.data
                })
                // alert(res.message);
            } else {
                this.setState({
                    isLoading: false,
                }, () => {
                    if (res && res.error) {
                        alert(res.error)
                    }
                })
            }

        }).catch(err => {
            this.setState({
                isLoading: false,
            })
            setTimeout(() => {
                if (err) {
                    alert(JSON.stringify(err));
                }
            }, 100);
        });

    }

    render() {

        return (
            <View style={{ flex: 1 }} >
                <NavigationEvents
                    onWillFocus={() => {
                        this.setState({ renderList: !this.state.renderList }, () => {
                            // alert("ID:"+this.state.user_id)
                            if (this.state.user_id != "" && this.state.user_id != null) {

                                this.callLoadAddressApi(this.state.user_id)
                            }
                        })
                        //Call whatever logic or dispatch redux actions and update the screen!
                    }}
                />
                <TouchableOpacity
                    style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        position: 'absolute',
                        bottom: (Platform.OS === Constants.PLATFORM.ios) ? 20 : 15,
                        right: 15,
                        backgroundColor: Constants.color.primary,
                        borderRadius: 100,
                        padding: 20,
                        zIndex: 1
                    }}
                    onPress={() => {
                        this.props.navigation.navigate('AddAddress');
                    }}
                >
                    <Image style={{ width: 15, height: 15 }} resizeMode='contain' source={Images.ic_add_white}></Image>
                </TouchableOpacity>
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
                {this.renderToolbar()}
                <View style={{ flex: 1 }}>

                    <View style={styles.loginForm}>
                        <TouchableOpacity
                            style={{
                                alignItems: 'center',
                                flexDirection: "row",
                                margin: 13,
                                alignSelf: 'flex-start'
                            }}
                            onPress={() => {
                                this.props.navigation.navigate('AddAddress');
                            }}  >
                            <View
                                style={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: Constants.color.primary,
                                    borderRadius: 100,
                                    height: 30,
                                    flexDirection: "row",
                                    width: 30,
                                }}  >
                                <Image style={{ width: 20, height: 20 }} resizeMode='contain' source={Images.ic_location_white}></Image>
                            </View>
                            <TextBold title={strings.side_menu_address} textStyle={{ fontSize: 14, color: Constants.color.black, marginHorizontal: 10 }} />
                        </TouchableOpacity>
                        <ScrollView contentInsetAdjustmentBehavior="automatic">
                            {this._addresslist()}
                        </ScrollView>
                    </View>
                </View>
                {this.renderProgressBar()}
            </View>
        );
    }
}

function mapStateToProps(state) {
    return {
        userData: state.userData,
        internet: state.internet,
    }
}

export default connect(mapStateToProps, { isLoginAction, userDataAction })(SelectDeliveryAddress)





