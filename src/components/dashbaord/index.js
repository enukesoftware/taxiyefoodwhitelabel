import React, { Component } from 'react'
import { Platform, StyleSheet, View, TouchableOpacity, Image, Alert, SafeAreaView, AsyncStorage, FlatList, ActivityIndicator, TextInput } from 'react-native';
import styles from './styles'
import { Constants, strings, Images, Fonts, GlobalStyle,callUpdateDeviceTokenApi } from '../../utils'
import { TextBold, TextLite, TextRegular, TextThin } from '../custom/text'
import { getRestaurantList } from '../../services/APIService'
import RestaurantItem from '../ListItem/RestaurantItem'
import { IndicatorViewPager } from 'rn-viewpager';
import CustomTabButtons from '../custom/custom-tab-buttons'
import RestaurantListPage from '../restaurant-list-page'
import { connect } from 'react-redux'
import { dashboardTabSelected } from '../../redux/actions'
import { createStackNavigator, createDrawerNavigator, NavigationEvents } from "react-navigation";
import CustomNavigationDrawer from '../custom/custom-navigation-drawer'
import NavigationService from '../../services/NavigationServices'

let locationAddress = '';
class Dashboard extends Component {

    static navigationOptions = ({ navigation }) => {
        console.log(`DASH_NAV:${JSON.stringify(navigation)}`)
        // oldSelectedCuisineList = navigation.getParam('cuisineList', []);

        return {
            header: null,
        }
    };


    // static navigationOptions = {
    //     header: null,

    // }

    search = () => {
        Alert.alert(strings.app_name, strings.error_msg, strings.ok)
    }

    state = {
        restaurantDataList: [],
        homeCookedFoodDataList: [],
        restaurantLoaderIsVisible: false,
        homeCookedLoaderIsVisible: false,
        filteredRestList: [],
        filteredHomeCookedList: [],
        restaurantPages: null,
        homeCookedPages: null,
        restNoData: false,
        homeCookedNoData: false,
        isSearchEnable: false,
        filters: null,
    }

    /* ----------function to pass in tab component----------*/
    onTabChange = key => value => {
        // this.setState({
        //     [key]: value
        // })

        console.log("TAB_CHANGE:KEY" + key + " VALUE:" + value)

        if (value == 0 && this.state.restaurantDataList.length == 0) {
            this.setState({
                restNoData: false,
            })
            this.callInitGetRestaurantListApi()
        }
        else if (value == 1 && this.state.homeCookedFoodDataList.length == 0) {
            this.setState({
                homeCookedNoData: false,
            })
            this.callInitGetHomeCookedListApi()
        }
    }




    constructor() {
        super()
        console.log("DASHBOARD:constructor")
    }

    componentWillMount() {
        console.log("DASHBOARD:componentWillMount")

    }

    componentDidMount() {
        // console.log("DASHBOARD_TOKEN:"+this.props.deviceToken)
        // if(this.props.isLogin){
        //     console.log("DASHBOARD_USER_DATA:"+JSON.stringify(this.props.userData))
        // }

        callUpdateDeviceTokenApi()
        AsyncStorage.getItem(Constants.STORAGE_KEY.filterRestaurants, (error, result) => {
            if (error) {
                console.log("ERROR:" + JSON.stringify(error))
            }
            else {
                if (result) {
                    result = JSON.parse(result)
                    this.setState({
                        filters: result
                    }, () => {
                        console.log("SAVED_DATA:" + JSON.stringify(this.state.filters))
                    })
                }
            }
        })
        console.log("DASHBOARD:componentDidMount")
        if (this.props.location) {
            locationAddress = JSON.parse(JSON.stringify(this.props.location)).address
            this.setState({
                restaurantLoaderIsVisible: true,
            }, () => {
                this.callInitGetRestaurantListApi();
            })
        }

    }


    //function to be pass to next screen and get result
    onFilterSelect = data => {
        console.log("FILTER_DATA:" + JSON.stringify(data))
        this.setState({
            filters: data.filters,
            restaurantDataList: [],
            homeCookedFoodDataList: [],
            // restaurantLoaderIsVisible: true,
            // homeCookedLoaderIsVisible: true,
            filteredRestList: [],
            filteredHomeCookedList: [],
            restaurantPages: null,
            homeCookedPages: null,
        }, () => {
            console.log("SAVED_DATA:" + JSON.stringify(this.state.filters))
            if (this.props.tabSelected == 0) {
                this.setState({
                    restaurantLoaderIsVisible: true
                })
                this.callInitGetRestaurantListApi()

            } else if (this.props.tabSelected == 1) {
                this.setState({
                    homeCookedLoaderIsVisible: true
                })
                this.callInitGetHomeCookedListApi()
            }
        })

    };

    onLocationChange = () => {
        console.log("LAST_ADDRESS:" + locationAddress);
        console.log("NEW_ADDRESS:" + JSON.stringify(this.props.location));
        if (locationAddress != this.props.location.address) {
            console.log("NOT_EQUAL:")
            locationAddress = JSON.parse(JSON.stringify(this.props.location)).address
            this.setState({
                restaurantLoaderIsVisible: true,
                restaurantDataList: [],
                homeCookedFoodDataList: [],
                filteredRestList: [],
                filteredHomeCookedList: [],
                restaurantPages: null,
                homeCookedPages: null,
            }, () => {
                if (this.props.tabSelected == 0) {
                    this.callInitGetRestaurantListApi();
                } else if (this.props.tabSelected == 1) {
                    this.callInitGetHomeCookedListApi()
                }
            })



        } else {
            console.log("EQUAL:")
            this.setState({
                restaurantLoaderIsVisible: false,
                homeCookedLoaderIsVisible: false,
            })
        }
    }

    // componentDidMount(){
    //     console.log(`DASHBOARD:componentDidMount:${JSON.stringify(this.props)}`)

    //     if(this.props.location){
    //         this.callGetRestaurantListApi();
    //     }
    // }


    openDrawer = () => {
        this.props.navigation.openDrawer();
    }

    closeDrawer = () => {
        this.props.navigation.closeDrawer();
    }

    callInitGetRestaurantListApi = () => {
        if (!this.props.internet) {
            console.log("INTERNET1")
            alert(strings.message_lno_internet)
            // this.setState({
            //     restNoData: false,
            //     restaurantLoaderIsVisible: false,
            // })
            return
        }
        this.setState({ restaurantLoaderIsVisible: true })
        let param = {
            lat: this.props.location.latitude,
            long: this.props.location.longitude,
            restauranttitle: '',
            restaurantlimit: 10,
            filterRestaurants: this.state.filters,
        }

        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.restaurantList + "/0" + "?page=1"

        getRestaurantList(url, param).then(res => {

            console.log("INIT_REST_API_RES:" + JSON.stringify(res))
            if (res && res.success && res.restaurant) {
                this.setState({
                    restaurantPages: res.restaurant,
                })
            }
            if (res && res.success && res.restaurant && res.restaurant.data && res.restaurant.data.length > 0) {

                this.setState({
                    restNoData: false,
                    restaurantDataList: res.restaurant.data,
                    filteredRestList: res.restaurant.data,
                    restaurantLoaderIsVisible: false,
                }, () => {
                    if (res.admin && res.admin.length > 0) {
                        AsyncStorage.setItem(Constants.STORAGE_KEY.adminData, JSON.stringify(res.admin[0]));

                    }
                })
            } else {
                this.setState({
                    restNoData: true,
                    restaurantLoaderIsVisible: false,
                })
            }

            //  this.props.savehotellist(res)
        }).catch(err => {
            this.setState({
                restNoData: true,
                restaurantLoaderIsVisible: false,
            })
            setTimeout(() => {
                if (err) {
                    // err = JSON.parse(err)
                    if (err != '') {
                        alert(err);
                    }
                }
            }, 100);
        });
    }

    callInitGetHomeCookedListApi = () => {

        if (!this.props.internet) {
            console.log("INTERNET2")
            alert(strings.message_lno_internet)
            // this.setState({
            //     homeCookedNoData: false,
            //     restaurantLoaderIsVisible: false,
            // })
            return
        }
        this.setState({ homeCookedLoaderIsVisible: true })
        let param = {
            lat: this.props.location.latitude,
            long: this.props.location.longitude,
            restauranttitle: '',
            restaurantlimit: 10,
            filterRestaurants: this.state.filters,
        }

        let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.restaurantList + "/1" + "?page=1"

        getRestaurantList(url, param).then(res => {

            console.log("INIT_HOME_COOKED_API_RES:" + JSON.stringify(res))
            if (res && res.success && res.restaurant) {
                this.setState({
                    homeCookedPages: res.restaurant,
                })
            }
            if (res && res.success && res.restaurant && res.restaurant.data && res.restaurant.data.length > 0) {
                this.setState({
                    homeCookedNoData: false,
                    homeCookedFoodDataList: res.restaurant.data,
                    filteredHomeCookedList: res.restaurant.data,
                    homeCookedLoaderIsVisible: false,
                })
            } else {
                this.setState({
                    homeCookedNoData: true,
                    homeCookedLoaderIsVisible: false,
                })
            }

            //  this.props.savehotellist(res)
        }).catch(err => {
            this.setState({
                homeCookedNoData: true,
                homeCookedLoaderIsVisible: false,
            })
            setTimeout(() => {
                alert(JSON.stringify(err));
            }, 100);
        });
    }

    callGetRestaurantListApi = () => {
        if (!this.props.internet) {
            console.log("INTERNET3")
            alert(strings.message_lno_internet)
            return
        }
        if (this.state.isSearchEnable) {
            return
        }
        this.setState({
            restaurantLoaderIsVisible: true,
        })

        let param = {
            lat: this.props.location.latitude,
            long: this.props.location.longitude,
            restauranttitle: '',
            restaurantlimit: 10,
            filterRestaurants: this.state.filters,
        }

        let url = ''
        if (this.state.restaurantPages) {
            if (this.state.restaurantPages.current_page && this.state.restaurantPages.last_page && !(this.state.restaurantPages.current_page === this.state.restaurantPages.last_page)) {
                url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.restaurantList + "/0" + "?page=" + (parseInt(this.state.restaurantPages.current_page) + 1)
            }
        }

        // let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.restaurantList + "/0" + "?page=1"


        if (url === '') {
            this.setState({
                restaurantLoaderIsVisible: false,
            })
            return
        }

        getRestaurantList(url, param).then(res => {

            if (res && res.success && res.restaurant) {
                this.setState({
                    restaurantPages: res.restaurant,
                })
            }
            if (res && res.success && res.restaurant && res.restaurant.data && res.restaurant.data.length > 0) {
                console.log("REST_API_RES:" + JSON.stringify(res))
                this.setState(prevState => ({
                    restaurantDataList: [...prevState.restaurantDataList, ...res.restaurant.data],
                    filteredRestList: [...prevState.restaurantDataList, ...res.restaurant.data],
                    restaurantLoaderIsVisible: false,
                }))
            } else {
                this.setState({
                    restaurantLoaderIsVisible: false,
                })
            }

            //  this.props.savehotellist(res)
        }).catch(err => {
            this.setState({
                restaurantLoaderIsVisible: false,
            })
            setTimeout(() => {
                alert(JSON.stringify(err));
            }, 100);
        });
    }

    callGetHomeCookedListApi = () => {
        if (!this.props.internet) {
            console.log("INTERNET4")
            alert(strings.message_lno_internet)
            return
        }
        if (this.state.isSearchEnable) {
            return
        }

        this.setState({
            homeCookedLoaderIsVisible: true,
        })

        let param = {
            lat: this.props.location.latitude,
            long: this.props.location.longitude,
            restauranttitle: '',
            restaurantlimit: 10,
            filterRestaurants: this.state.filters,
        }

        let url = ''
        if (this.state.homeCookedPages) {
            if (this.state.homeCookedPages.current_page && this.state.homeCookedPages.last_page && !(this.state.homeCookedPages.current_page === this.state.homeCookedPages.last_page)) {
                url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.restaurantList + "/1" + "?page=" + (parseInt(this.state.homeCookedPages.current_page) + 1)
            }
        }

        // let url = Constants.URL.baseURL + '/' + Constants.language.english + '/' + Constants.URL.vesrion + '/' + Constants.URL.restaurantList + "/0" + "?page=1"



        if (url === '') {
            this.setState({
                homeCookedLoaderIsVisible: false,
            })
            return
        }

        getRestaurantList(url, param).then(res => {

            console.log("HOME_COOKED_API_RES:" + JSON.stringify(res))
            if (res && res.success && res.restaurant) {
                this.setState({
                    homeCookedPages: res.restaurant,
                })
            }
            if (res && res.success && res.restaurant && res.restaurant.data && res.restaurant.data.length > 0) {
                this.setState(prevState => ({
                    homeCookedFoodDataList: [...this.prevState.homeCookedFoodDataList, ...res.restaurant.data],
                    filteredHomeCookedList: [...this.prevState.homeCookedFoodDataList, ...res.restaurant.data],
                    homeCookedLoaderIsVisible: false,
                }))
            } else {
                this.setState({
                    homeCookedLoaderIsVisible: false,
                })
            }

            //  this.props.savehotellist(res)
        }).catch(err => {
            this.setState({
                homeCookedLoaderIsVisible: false,
            })
            setTimeout(() => {
                alert(JSON.stringify(err));
            }, 100);
        });
    }



    changeTab = () => {
        console.log("CHANGE_TAB:" + this.props.tabSelected)
        if (this.pager != null) {
            this.pager.setPage(this.props.tabSelected)

        }

    }

    renderTabState() {
        return (
            <View>
                {this.changeTab()}
            </View>
        )
    }

    renderProgressBar() {
        if (this.props.tabSelected == 0 && this.state.restaurantLoaderIsVisible) {
            return (
                <View style={styles.activityIndicatorView}>
                    <View style={styles.activityIndicatorWrapper}>
                        <ActivityIndicator
                            size={"large"}
                            color={Constants.color.primary}
                            animating={this.state.loading} />
                    </View>
                </View>

            )
        } else if (this.props.tabSelected == 1 && this.state.homeCookedLoaderIsVisible) {
            return (
                <View style={styles.activityIndicatorView}>
                    <View style={styles.activityIndicatorWrapper}>
                        <ActivityIndicator
                            size={"large"}
                            color={Constants.color.primary}
                            animating={this.state.loading} />
                    </View>
                </View>

            )
        } else {
            return (
                <View />
            )
        }

    }

    onScrollHandler = () => {
        console.log("ONSCROLL")
        if(this.state.isSearchEnable){
            return
        }
        if (this.props.tabSelected == 0) {
            this.callGetRestaurantListApi();
        } else if (this.props.tabSelected == 1) {
            this.callGetHomeCookedListApi();
        }
    }

    handleSearchInput = (e) => {
        if (this.props.tabSelected == 0) {
            let text = e.toLowerCase()
            let fullRestList = this.state.restaurantDataList;
            let filteredRestList = fullRestList.filter((item) => { // search from a full list, and not from a previous search results list
                if (item.name.toLowerCase().match(text))
                    return item;
            })
            if (!text || text === '') {
                this.setState({
                    filteredRestList: filteredRestList,
                    restNoData: false,
                })
            } else if (!filteredRestList.length) {
                // set no data flag to true so as to render flatlist conditionally
                this.setState({
                    restNoData: true
                })
            }
            else if (Array.isArray(filteredRestList)) {
                this.setState({
                    restNoData: false,
                    filteredRestList: filteredRestList
                })
            }


        } else if (this.props.tabSelected == 1) {
            let text = e.toLowerCase()
            let fullList = this.state.homeCookedFoodDataList;
            let filteredList = fullList.filter((item) => { // search from a full list, and not from a previous search results list
                if (item.name.toLowerCase().match(text))
                    return item;
            })
            if (!text || text === '') {
                this.setState({
                    filteredHomeCookedList: fullList,
                    homeCookedNoData: false,
                })
            } else if (!filteredList.length) {
                // set no data flag to true so as to render flatlist conditionally
                this.setState({
                    homeCookedNoData: true
                })
            }
            else if (Array.isArray(filteredList)) {
                this.setState({
                    homeCookedNoData: false,
                    filteredHomeCookedList: filteredList
                })
            }
        }
    }

    renderRestaurantItem = () => {
        if (this.props.tabSelected == 0) {

            if (this.state.restNoData) {
                return (
                    <View style={{ width: '100%', flex: 1, alignItems: 'center', justifyContent: 'center', paddingBottom: '15%' }}>
                        <TextRegular title={strings.no_data_found} textStyle={{ fontSize: 20 }} />
                        {this.renderProgressBar()}
                    </View>
                )
            } else {
                return (
                    <View style={{ width: '100%', flex: 1 }}>
                        <FlatList
                            // onRefresh={this.mRefresh}
                            // refreshing={this.state.isRefreshing}
                            data={this.state.filteredRestList}
                            keyExtractor={item => item.id}
                            // ListHeaderComponent={this.renderHeader}
                            onEndReached={this.onScrollHandler}
                            onEndReachedThreshold={0.1}
                            renderItem={({ item }) => <RestaurantItem item={item} />}
                        />
                        {this.renderProgressBar()}
                    </View>
                )
            }


        } else {
            return
        }

    }

    renderHomeCookedItem = () => {
        if (this.props.tabSelected == 1) {

            if (this.state.homeCookedNoData) {
                return (
                    <View style={{ width: '100%', flex: 1, alignItems: 'center', justifyContent: 'center', paddingBottom: '15%' }}>
                        <TextRegular title={strings.no_data_found} textStyle={{ fontSize: 20 }} />
                        {this.renderProgressBar()}
                    </View>
                )
            } else {
                return (
                    <View style={{ width: '100%', flex: 1 }}>
                        <FlatList
                            // onRefresh={this.mRefresh}
                            // refreshing={this.state.isRefreshing}
                            data={this.state.filteredHomeCookedList}
                            keyExtractor={item => item.id}
                            // ListHeaderComponent={this.renderHeader}
                            onEndReached={() => this.onScrollHandler}
                            onEndReachedThreshold={0.1}
                            renderItem={({ item }) => <RestaurantItem item={item} />}
                        />

                        {this.renderProgressBar()}
                    </View>
                )
            }

        } else {
            return
        }

    }

    renderTabView() {
        if (this.state.isSearchEnable) {
            return
        }
        return (
            <View>
                <CustomTabButtons onTabChange={this.onTabChange('tab')} />
            </View>
        )
    }

    renderSearchView() {
        if (!this.state.isSearchEnable) {
            return (
                <View style={{ marginLeft: 15, width: '75%' }}>
                    <TextBold title={this.props.location.city}
                        numberOfLines={1}
                        textStyle={{
                            paddingRight: 10,
                            color: Constants.color.fontWhite,
                            fontSize: Platform.OS === Constants.PLATFORM.android ? Constants.fontSize.NormalX : Constants.fontSize.NormalXX
                        }} />
                    <TextRegular numberOfLines={1} title={this.props.location.address}
                        textStyle={{
                            paddingRight: 10,
                            color: Constants.color.fontWhite,
                            fontSize: Platform.OS === Constants.PLATFORM.android ? Constants.fontSize.SmallXXX : Constants.fontSize.NormalX
                        }} />
                </View>
            )
        }
        return (
            <View
                style={{
                    flexDirection: 'row', borderBottomColor: Constants.color.primary,
                    borderBottomWidth: 1,
                }}>
                <TextInput style={
                    {
                        width: '90%',
                        paddingHorizontal: 10,
                        fontFamily: Fonts.Regular,
                        color: Constants.color.fontWhite,
                        fontSize: Platform.OS === Constants.PLATFORM.android ? Constants.fontSize.NormalX : Constants.fontSize.NormalXX
                    }
                }
                    autoFocus={true}
                    onChangeText={(text) => this.handleSearchInput(text)}>
                </TextInput>


            </View>
        )
    }

    renderSearchMenuButton() {
        if (this.state.isSearchEnable) {
            return (
                <View>
                    <TouchableOpacity activeOpacity={0.8}
                        onPress={() => this.setState(prevState => ({
                            restNoData: (prevState.restaurantDataList && prevState.restaurantDataList.length>0)?false:true,
                            homeCookedNoData:(prevState.homeCookedFoodDataList && prevState.homeCookedFoodDataList.length >0)?false:true,
                            isSearchEnable: false,
                            filteredRestList: prevState.restaurantDataList,
                            filteredHomeCookedList: prevState.homeCookedFoodDataList
                        }))}
                    >
                        <Image source={Images.ic_close_white}
                            style={{ marginHorizontal: 15, height: 25, width: 25 }}
                        ></Image>
                    </TouchableOpacity>
                </View>

            )
        } else {
            return (
                <View>
                    <TouchableOpacity activeOpacity={0.8}
                        onPress={() => this.setState({
                            isSearchEnable: true
                        })}
                    >
                        <Image source={Images.ic_search_white}
                            style={{ marginHorizontal: 15, height: 25, width: 25 }}
                        ></Image>
                    </TouchableOpacity>
                </View>

            )
        }

    }

    render() {
        console.log(`DASHBOARD:render:${JSON.stringify(this.props)}`)
        // let toolbarHeight = (Platform.OS === Constants.PLATFORM.ios) ? Constants.AppConstant.statusBarHeight + 60 : Constants.AppConstant.statusBarHeight + 25
        let toolbarHeight = (Platform.OS === Constants.PLATFORM.ios) ? Constants.TOOLBAR_HEIGHT.ios : Constants.TOOLBAR_HEIGHT.android

        return (
            <View style={styles.container}>
                {/* ---------- It wiil load every time user come to this page ---------- */}
                <NavigationEvents
                    onWillFocus={() => {
                        // this.setState({
                        //     restaurantLoaderIsVisible: this.props.tabSelected == 0 ? true : false,
                        //     homeCookedLoaderIsVisible: this.props.tabSelected == 1 ? true : false,
                        // })
                        this.onLocationChange()
                    }}
                />
                <SafeAreaView style={{ backgroundColor: Constants.color.primary }}></SafeAreaView>
                <View style={[GlobalStyle.toolbar, { height: toolbarHeight, backgroundColor: Constants.color.primary, justifyContent: 'center', }]} >
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', width: '80%' }}>
                            <TouchableOpacity activeOpacity={0.8} onPress={() => this.openDrawer()}>
                                <Image source={Images.ic_menu_drawer}
                                    style={{ marginLeft: 15, height: 22, width: 22, }}
                                ></Image>
                            </TouchableOpacity>
                            {this.renderSearchView()}
                        </View>


                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'flex-end', width: '20%' }}>

                            {this.renderSearchMenuButton()}
                            <TouchableOpacity activeOpacity={0.8}
                                onPress={() => NavigationService.navigate("Filter", { onFilterSelect: this.onFilterSelect })}>
                                <Image source={Images.ic_filter}
                                    style={{ marginRight: 15, height: 25, width: 25 }}
                                ></Image>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                {/* </SafeAreaView>  */}

                {/* {this.renderSearchView()} */}
                {this.renderTabView()}

                <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
                    {this.renderRestaurantItem()}
                    {this.renderHomeCookedItem()}
                    {/* <RestaurantListPage /> */}
                    {/* <HomeCookedListPage /> */}
                    {/* <IndicatorViewPager
                        ref={pager => this.pager = pager}
                        scrollEnabled={true}
                        style={{ flex: 1 }}
                        onPageSelected={(position)=>{this.props.dashboardTabSelected(position.position)}}
                        
                    >
                        <View>
                            <RestaurantListPage selected={0}/>
                        </View>
                        <View >
                            <RestaurantListPage selected={1}/>
                        </View>
                    </IndicatorViewPager>
                    {this.renderTabState()} */}
                </View>
            </View>
        );
    };
}

function mapStateToProps(state) {
    // console.log(`DASHBOARD_STATE:${JSON.stringify(state)}`)
    return {
        location: state.location,
        tabSelected: state.dashboardTabSelected,
        internet: state.internet,
        isLogin:state.isLogin,
        userData:state.userData,
        deviceToken:state.deviceToken
        // state
    }
}

export default connect(mapStateToProps, {})(Dashboard)