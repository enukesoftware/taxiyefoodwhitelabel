import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, StatusBar, SafeAreaView,Button } from 'react-native';
// import styles from './styles'
// import { strings } from '../utils/strings'
import {connect} from 'react-redux'
import {internetConnected,internetDisconnected} from '../redux/actions'
// import {Fonts} from '../utils/fonts'
import {TextThin} from './custom/text'
import {Constants, strings,Fonts} from '../utils'

class MyComponent extends Component {

    static navigationOptions = {
        title: 'Restaurent Name',
        headerStyle: {
            backgroundColor: Constants.color.primary,
        },
        headerTitleStyle: {
            color: 'white',
            fontWeight: 'bold'
        },
    };

    constructor() {
        super()
    }

    render() {
        console.log('APP',this.props)
        return (
            <View style={styles.container}>
                {/* <StatusBar
                    barStyle="light-content"
                    backgroundColor={Constants.color.primaryDark} /> */}
                <TextThin title='Hello' textStyle={{fontSize:30}}></TextThin>
                <Text style={styles.welcome}>{strings.app_name}</Text>
                <Text style={styles.instructions}>{this.props.isInternetConnected.toString()}</Text>
                {/* <Text style={styles.instructions}>{instructions}</Text> */}
                <Button title='Connect' onPress={this.props.internetConnected}></Button>
                <Button title='Disconnect' onPress={this.props.internetDisconnected}></Button>

            </View>
        );
    };
}

function mapStateToProps(state){
      return{
        isInternetConnected:state.internet,
      }
  }

// export default MyComponent

export default connect(mapStateToProps,{internetConnected,internetDisconnected})(MyComponent)


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        fontFamily:Fonts.Regular
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});