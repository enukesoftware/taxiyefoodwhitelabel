import { StyleSheet } from 'react-native';
import { Fonts } from '../../utils/fonts'
import { Constants } from '../../utils'


export default StyleSheet.create({
  contentContainer: {
  },
  safeStyle: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    height: '100%',
    width: '100%',
  },
  container: {
    flex: 1,
    paddingTop: Constants.AppConstant.statusBarHeight,
  },
  infoContainer: {
    flex: 1,
    padding: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  textContainer: {
    flex: 1,
  },
  map: {
    // ...StyleSheet.absoluteFillObject,
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },
  bottomSafeStyle: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: '100%'
  },
  imageInfo: {
    height: 90,
    width: 90,
    margin: 15,
  },
  textInfo: {
    color: 'white',
    fontSize: 14,
  },
  weekRow: {
    flexDirection: "row",
    paddingHorizontal: 20,
  },
  viewHead: {
    backgroundColor: '#808080',
    width: '100%',
    height: 1,
    marginTop: 25
  },

  textHead: {
    color: 'black',
    margin: 20,
    marginTop: 25,
    marginBottom: 10,
  },
  textData: {
    color: '#808080',
    marginHorizontal: 40,
    marginVertical: 5,
  },
  ratingContainer: {
    flex: 1,
    width: '90%',
    marginLeft: '5%',
    backgroundColor: Constants.color.white,
    marginTop: 5,
    marginBottom: 5,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: Constants.color.gray,
    shadowRadius: 5,
    shadowOpacity: 0.5,
    elevation: 10
  },
  ratingInnerContainer: {
    alignItems: 'center',
    // justifyContent: 'center',
    flex: 1,
    width: '50%',

  },
  ratingUserProfileImage: {
    height: 50,
    width: 50,
    // resizeMode: 'stretch',
    borderRadius: 25
  },
  ratingUsername: {
    color: 'black',
    marginTop: 5,
  },
  ratingComment: {
    color: '#808080',
    marginVertical: 5,
  },

})